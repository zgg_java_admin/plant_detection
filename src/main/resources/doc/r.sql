/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50716
Source Host           : localhost:3306
Source Database       : mybatis-plus

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2019-12-29 19:49:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin_user`
-- ----------------------------
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user` (
  `id` varchar(32) NOT NULL,
  `login_name` varchar(32) DEFAULT NULL COMMENT '登录名',
  `pwd` varchar(32) DEFAULT NULL COMMENT '密码',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of admin_user
-- ----------------------------
INSERT INTO `admin_user` VALUES ('1', 'admin', 'admin', '超级管理员', '2019-12-23 14:51:34');
INSERT INTO `admin_user` VALUES ('c277017ec796451799ac8af6cc73dedf', '4', '12', '33', null);

-- ----------------------------
-- Table structure for `article`
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` varchar(32) NOT NULL,
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `summary` varchar(200) DEFAULT NULL COMMENT '摘要',
  `content` longtext COMMENT '内容',
  `type` int(11) DEFAULT NULL COMMENT '1 : 植被 2害虫 3果蔬',
  `child_type` varchar(32) DEFAULT NULL COMMENT '二级分类ID',
  `label` text COMMENT '标签',
  `user_id` varchar(64) DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '发布时间',
  `status` int(11) DEFAULT NULL COMMENT '1 审核通过   2审核拒绝',
  `check_result` text COMMENT '检查结果',
  `top` tinyint(1) DEFAULT NULL COMMENT '是否置顶',
  `cover` varchar(500) DEFAULT NULL COMMENT '封面',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章表';

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('08281edd5b074fa483cff4044d44abc7', '222', '3333', '<p>124124124124</p>', '1', '17a094b17cb04fb18a78420560315b3a', '44124', '-1', '2019-12-24 16:10:59', '1', null, '1', '/img/pic?pictureName=12093859401420308508ZWZA4F%5DE%40DQ%7BEUQKII4H%601.png');
INSERT INTO `article` VALUES ('1', '娑罗双树', '示范法', '<p><span class=\"bjh-p\">12月25日消息，据外媒报道，科技巨头们越来越多地自主设计半导体芯片，以优化AI功能、提高服务器性能及延长电池续航时间等。谷歌拥有张量处理单元（TPU），苹果拥有A13仿生芯片，亚马逊拥有Graviton 2。然而，这些巨头们都缺少能够帮助生产新芯片的工艺。为了帮助它们，三星电子公司计划在未来十年投入1160亿美元资金以推动芯片制造业务扩张。</span></p><p><img class=\"large\" src=\"http://t10.baidu.com/it/u=1782150138,2899989519&fm=173&app=25&f=JPEG?w=640&h=427&s=1C9660950C52EDCA10A518D40300C0B0\"/></p><p><span class=\"bjh-p\">图：三星公司位于韩国华城市的园区</span></p><p><span class=\"bjh-p\">三星正大举投资于半导体微型化的下一代技术，也被称为极紫外光刻(EUV)技术。这是三星迄今为止尝试过的最昂贵制造业务升级，也是一次冒险的尝试，目的是让自己不再局限于现有的半导体生产业务，也不再满足于只充当代工和逻辑芯片行业的领先者，即使这个行业的规模已达2500亿美元。</span></p><p><span class=\"bjh-p\">三星代工业务执行副总裁尹永植最近在首尔举行的论坛上表示：“一个新的市场正在崛起。像亚马逊、谷歌和阿里巴巴这样在硅设计方面缺乏经验的公司，正寻求用自己的概念想法来制造芯片，以提升他们的服务水平。我认为，这将为我们的非存储芯片业务带来重大突破。“</span></p><p><span class=\"bjh-p\">在这个不断增长的领域，三星相对处于劣势。市场研究公司TrendForce的数据显示，芯片代工业务，如为谷歌和高通等公司制造芯片，始终由台积电(TSMC)主导，其市场占有率超过一半，而三星的市场份额仅为18%。</span></p><p><span class=\"bjh-p\">台积电还从三星手中接过了苹果A系列处理器的代工业务，尽管三星是苹果最初的生产合作伙伴。三星计划在未来十年每年投入超过100亿美元用于设备研发，但台积电的雄心更大，今明两年的资本支出约为140亿美元。</span></p><p><span class=\"bjh-p\">野村金融投资公司泛亚洲科技部主管CW Chung在评估三星的成功机会时表示：“这不仅仅是意愿的问题，芯片制造就像是一门合成艺术。除非对基础设施提供全面支持，否则这将是一个难以实现的目标。”</span></p><p><span class=\"bjh-p\">为了赢得客户青睐，三星高管正</span></p><p><span class=\"bjh-p\"><br/></span></p><p><span class=\"bjh-p\"><br/></span></p><p><span class=\"bjh-p\">在圣何塞、慕尼黑以及上海等大城市举行巡回演讲，举办代工论坛并洽谈交易。</span></p><p><span class=\"bjh-p\">三星代工业务总裁兼总经理Es Jung今年早些时候陪同韩国总统文在寅和李在镕，为耗资170亿美元的EUV工厂揭幕时表示：“EUV设备绘制线条的复杂性与建造宇宙飞船差不多。”这家工厂计划于2020年2月开始批量生产。</span></p><p><span class=\"bjh-p\">ASML Holding NV的EUV机器售价1.72亿美元，三星正在华城安装数十台EUV机器，以努力在这项技术上取得领先地位。台积电和三星都有望在新的一年里实现EUV的5纳米生产工艺，这意味着它们在这个日益扩大的市场上竞争将日益激烈。</span></p><p><span class=\"bjh-p\">花旗集团公布的研究报告显示，一旦台积电和三星加速并实现规模经济，整个工艺周期的升级时间可能会减少20%，代工产能产出则会增加25%。</span></p><p><span class=\"bjh-p\">现代汽车证券公司高级副总裁格雷格·罗(Greg Roh)表示：“随着我们进入5G时代，台积电陷入极度忙碌状态，新产品的订单蜂拥而至。对三星来说，这也是个很好的机会，通过提供更低价格和更短交货时间表来满足客户的需求，从而扩大他们的市场份额。”</span></p><p><img class=\"large\" src=\"http://t11.baidu.com/it/u=2609429230,967478339&fm=173&app=25&f=JPEG?w=640&h=335&s=D34360A6C88428C64D23501A0300E09A\"/></p><p><span class=\"bjh-p\">据直接了解此事的三星高管透露，三星正在与主要客户合作设计和制造定制芯片，这项工作已经开始增加其收入。硅谷和中国对定制处理器的推动正在打开新的机会，三星已经为此建立了合作关系，最近宣布将于明年初为百度生产AI芯片就证明了这一点。</span></p><p><span class=\"bjh-p\">三星的管理人员认为，该公司拥有竞争优势，因为它在制造芯片和设备方面的经验都很丰富。因此，三星能够预见和解决其客户面临的工程要求。三星认为，其另一张王牌是将内存和逻辑芯片封装到单个模块中的能力，从而提高功率和空间效率。</span></p><p><span class=\"bjh-p\">然而，分析人士警告说，有些公司对将生产外包给消费电子市场的直接竞争对手持谨慎态度，担心三星学习并在自己的产品中复制他们的芯片设计。分析师们称：“归根结底，三星逻辑芯片业务的成功取决于其市场定位。在代工方面，三星需要消除客户的怀疑，让他们不再将其视为逻辑芯片业务的潜在竞争对手。”</span></p><p><span class=\"bjh-p\">三星正在与智能手机制造竞争对手接触，并已同意向vivo出售5G Exynos芯片。与此同时，该公司将使用相同的EUV工艺制造高通的5G移动芯片组。</span></p><p><span class=\"bjh-p\">另一方面，三星正与代工客户索尼在不断增长的图像传感器市场展开竞争，今年发布了史无前例的1.08亿像素智能手机摄像头。彭博智库分析师安希·赖(Anthea Lai)表示：“搭上行业繁荣的顺风车后，我认为三星的CMOS图像传感器业务将继续表现良好。”</span></p><p><span class=\"bjh-p\">如果三星能够在技术上取得进步，它应该会发现，其多样化的半导体产品不会缺少客户。尽管中国越来越多地向国内供应商寻求各种技术支持，但EUV芯片的更高效率可能是帮助三星从中国招揽业务的关键。</span></p><p><span class=\"bjh-p\">TrendForce分析师指出：“各大公司对内部芯片的需求增加，这对晶圆代工行业的成长来说是个好消息。”</span></p><p><span class=\"bjh-p\">【来源：网易科技】</span></p><p><audio></audio></p><p><img src=\"/img/pic?pictureName=121080577640177664212100925249209589781.png\"/></p>', '1', '17a094b17cb04fb18a78420560315b3a', '虎皮兰,我开', '784972358981328902', '2019-12-24 14:05:36', '1', '2222', '0', '/img/pic?pictureName=12093859401420308508ZWZA4F%5DE%40DQ%7BEUQKII4H%601.png');
INSERT INTO `article` VALUES ('12db592f58364cefabf4989c0e3d2b71', '同时搭建SLB与CDN', '同时搭建SLB与CDN', '<h3 style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-weight: 500; color: rgb(0, 0, 0); font-size: 14px; height: auto; line-height: 17px;\"><a target=\"_blank\" href=\"http://bbs.aliyun.com/read/180564.html\" class=\"y-blue\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; text-decoration-line: none; transition: color 0.2s ease 0s; color: rgb(55, 61, 65); background-color: transparent; font-weight: inherit; font-size: 16px; line-height: 24px; font-family: PingFangSC-Regular, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">系统同时搭建<span style=\"box-sizing: inherit; margin: 0px; padding: 0px; color: rgb(0, 193, 222);\">SLB</span>与CDN，<span style=\"box-sizing: inherit; margin: 0px; padding: 0px; color: rgb(0, 193, 222);\">SLB</span>走动态数据，CDN走静态资源，CDN的流量会通过<span style=\"box-sizing: inherit; margin: 0px; padding: 0px; color: rgb(0, 193, 222);\">SLB</span>吗</a></h3><p>如题，CDN的流量是直达到用户那里，还是需要通过<span style=\"box-sizing: inherit; margin: 0px; padding: 0px; color: rgb(255, 102, 0);\">SLB</span>再到用户那里？如果是需要通过<span style=\"box-sizing: inherit; margin: 0px; padding: 0px; color: rgb(255, 102, 0);\">SLB</span>，那是不是<span style=\"box-sizing: inherit; margin: 0px; padding: 0px; color: rgb(255, 102, 0);\">SLB</span>与CDN的流量费用要计双份？ ...</p><p><span class=\"source\" style=\"box-sizing: inherit; margin: 0px; padding: 0px;\">来自：</span>&nbsp;<a href=\"https://yq.aliyun.com/\" target=\"_blank\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; text-decoration-line: none; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">云栖社区</a>&nbsp;&gt;&nbsp;<a href=\"https://www.aliyun.com/ss/c2xi/b_bbs\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; text-decoration-line: none; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">论坛</a>&nbsp;<span class=\"s-ml\" style=\"box-sizing: inherit; margin: 0px 0px 0px 20px; padding: 0px;\">作者：<a href=\"https://www.aliyun.com/ss/c2xi/b_uid_1055356858721031\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; text-decoration-line: none; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">&nbsp;wldlzt519</a></span>&nbsp;<span class=\"s-ml\" style=\"box-sizing: inherit; margin: 0px 0px 0px 20px; padding: 0px;\">浏览：<span style=\"box-sizing: inherit; margin: 0px; padding: 0px; color: rgb(155, 158, 160);\">7432</span></span>&nbsp;<span class=\"s-ml\" style=\"box-sizing: inherit; margin: 0px 0px 0px 20px; padding: 0px;\">回复：<span style=\"box-sizing: inherit; margin: 0px; padding: 0px; color: rgb(155, 158, 160);\">6</span></span></p><h3 style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-weight: 500; color: rgb(0, 0, 0); font-size: 14px; height: auto; line-height: 17px;\"><a target=\"_blank\" href=\"https://help.aliyun.com/document_detail/113305.html\" class=\"y-blue\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; text-decoration-line: none; transition: color 0.2s ease 0s; color: rgb(55, 61, 65); background-color: transparent; font-weight: inherit; font-size: 16px; line-height: 24px; font-family: PingFangSC-Regular, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">绑定<span style=\"box-sizing: inherit; margin: 0px; padding: 0px; color: rgb(0, 193, 222);\">SLB</span>&nbsp;- Serverless 应用引擎</a></h3><p>在SAE中部署应用后，可以通过添加公网<span style=\"box-sizing: inherit; margin: 0px; padding: 0px; color: rgb(255, 102, 0);\">SLB</span>&nbsp;...</p><p><span class=\"source\" style=\"box-sizing: inherit; margin: 0px; padding: 0px;\">来自：</span>&nbsp;<a href=\"https://help.aliyun.com/\" target=\"_blank\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; text-decoration-line: none; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">文档</a><a style=\"box-sizing: inherit; margin: 0px; padding: 0px; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">&nbsp;&gt;&nbsp;</a><a href=\"https://help.aliyun.com/product/118957.html\" target=\"_blank\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; text-decoration-line: none; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">Serverless 应用引擎</a><a style=\"box-sizing: inherit; margin: 0px; padding: 0px; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">&nbsp;&gt;&nbsp;</a><a href=\"https://help.aliyun.com/knowledge_list/97783.html\" target=\"_blank\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; text-decoration-line: none; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">应用管理</a><a style=\"box-sizing: inherit; margin: 0px; padding: 0px; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\"></a></p><p><a style=\"box-sizing: inherit; margin: 0px; padding: 0px; transition: color 0.2s ease 0s; color: rgb(0, 162, 202); background-color: transparent;\"></a></p><p><a style=\"box-sizing: inherit; margin: 0px; padding: 0px; transition: color 0.2s ease 0s; color: rgb(0, 162, 202); background-color: rgb(255, 255, 255); font-family: PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif; font-size: 12px; white-space: normal;\"></a></p><p><a style=\"box-sizing: inherit; margin: 0px; padding: 0px; transition: color 0.2s ease 0s; color: rgb(0, 162, 202); background-color: transparent;\"></a></p><h3 style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-weight: 500; color: rgb(0, 0, 0); font-size: 14px; height: auto; line-height: 17px;\"><a style=\"box-sizing: inherit; margin: 0px; padding: 0px; transition: color 0.2s ease 0s; color: rgb(61, 71, 81); background-color: transparent; font-weight: inherit; font-size: 16px; line-height: 24px; font-family: PingFangSC-Regular, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\"></a><a target=\"_blank\" href=\"https://help.aliyun.com/document_detail/116217.html\" class=\"y-blue\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; text-decoration-line: none; transition: color 0.2s ease 0s; color: rgb(55, 61, 65); background-color: transparent; font-weight: inherit; font-size: 16px; line-height: 24px; font-family: PingFangSC-Regular, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\"><span style=\"box-sizing: inherit; margin: 0px; padding: 0px; color: rgb(0, 193, 222);\">SLB</span>小时数据 - 阿里云交易和账单管理API</a></h3><p>接口名QueryUserOmsData 描述查询<span style=\"box-sizing: inherit; margin: 0px; padding: 0px; color: rgb(255, 102, 0);\">SLB</span>产品按小时计量的数据信息。 请求参数 名称 类型 是否必填 说明 Table String 是&nbsp;<span style=\"box-sizing: inherit; margin: 0px; padding: 0px; color: rgb(255, 102, 0);\">slb</span>DataType String 是 计量数据时间类型。Hour：小时 ...</p><p><span class=\"source\" style=\"box-sizing: inherit; margin: 0px; padding: 0px;\">来自：</span>&nbsp;<a href=\"https://help.aliyun.com/\" target=\"_blank\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; text-decoration-line: none; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">文档</a><a style=\"box-sizing: inherit; margin: 0px; padding: 0px; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">&nbsp;&gt;&nbsp;</a><a href=\"https://help.aliyun.com/product/87964.html\" target=\"_blank\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; text-decoration-line: none; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">阿里云交易和账单管理API</a><a style=\"box-sizing: inherit; margin: 0px; padding: 0px; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">&nbsp;&gt;&nbsp;</a><a href=\"https://help.aliyun.com/knowledge_list/115225.html\" target=\"_blank\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; text-decoration-line: none; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">计量域</a><a style=\"box-sizing: inherit; margin: 0px; padding: 0px; transition: color 0.2s ease 0s; color: rgb(155, 158, 160); background-color: transparent; font-family: PingFangSC-Light, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\"></a></p><p><a style=\"box-sizing: inherit; margin: 0px; padding: 0px; transition: color 0.2s ease 0s; color: rgb(0, 162, 202); background-color: transparent;\"></a></p><p><a style=\"box-sizing: inherit; margin: 0px; padding: 0px; transition: color 0.2s ease 0s; color: rgb(0, 162, 202); background-color: rgb(255, 255, 255); font-family: PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif; font-size: 12px; white-space: normal;\"></a></p><p><a style=\"box-sizing: inherit; margin: 0px; padding: 0px; transition: color 0.2s ease 0s; color: rgb(0, 162, 202); background-color: transparent;\"></a></p><h3 style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-weight: 500; color: rgb(0, 0, 0); font-size: 14px; height: auto; line-height: 17px;\"><a style=\"box-sizing: inherit; margin: 0px; padding: 0px; transition: color 0.2s ease 0s; color: rgb(61, 71, 81); background-color: transparent; font-weight: inherit; font-size: 16px; line-height: 24px; font-family: PingFangSC-Regular, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\"></a><a target=\"_blank\" href=\"https://help.aliyun.com/document_detail/109661.html\" class=\"y-blue\" style=\"box-sizing: inherit; margin: 0px; padding: 0px; text-decoration-line: none; transition: color 0.2s ease 0s; color: rgb(55, 61, 65); background-color: transparent; font-weight: inherit; font-size: 16px; line-height: 24px; font-family: PingFangSC-Regular, PingFangSC, &quot;helvetica neue&quot;, &quot;hiragino sans gb&quot;, arial, &quot;microsoft yahei ui&quot;, &quot;microsoft yahei&quot;, simsun, sans-serif !important;\">为 Kubernetes 集群中的应用添加负载均衡</a></h3><p><br/></p>', '3', 'e231e28280a44d0b83ea4d1ed87d2bb8', '苹果', '784972358981328902', '2019-12-25 17:26:03', '1', null, '0', '/img/pic?pictureName=1209767240019492865mew0.png');
INSERT INTO `article` VALUES ('1b9b30fee9414eb9ae20e4a9e6e0427a', '哦你觉得', '哦你觉得', '<p>共产党</p>', '1', '17a094b17cb04fb18a78420560315b3a', '觉得,欧尼', '-1', '2019-12-28 14:10:55', '1', null, '0', '/img/pic?pictureName=121080523461514035312100933426298839062.png');
INSERT INTO `article` VALUES ('301e46a44a074b9c873f329df2e65b89', '444444', '44444', '<p>124124124</p>', '2', '13293cf1d3f44196b0de99ca4a398cc3', '4444', '-1', '2019-12-24 16:32:31', '1', null, '1', '/img/pic?pictureName=12093904028060835858ZWZA4F%5DE%40DQ%7BEUQKII4H%601.png');
INSERT INTO `article` VALUES ('50a29866aa9f40139c800a7ab415fe3b', '444444', '44444', '<p>124124124</p>', '2', 'a368c821f3494f2e95bd1cdd9c568528', '4444', '-1', '2019-12-24 16:28:42', '1', null, '1', '/img/pic?pictureName=12093904028060835858ZWZA4F%5DE%40DQ%7BEUQKII4H%601.png');
INSERT INTO `article` VALUES ('7ce11cc879744c069e3b7b35ea7c34e8', '等等', '124124', '<p>沙发沙发1241412</p>', '1', '17a094b17cb04fb18a78420560315b3a', '124124124', '784972358981328902', '2019-12-28 14:18:52', '1', null, '0', '/img/pic?pictureName=121080707287792025712101007849819381772.png');
INSERT INTO `article` VALUES ('9fcb3df2442e4559bea1ca1159ffaecd', '个熊', '33333', '<p>共产党</p>', '2', '13293cf1d3f44196b0de99ca4a398cc3', '42124', '784972358981328902', '2019-12-28 14:15:57', '1', null, '0', '/img/pic?pictureName=121080647703509401712100988195688325146.png');

-- ----------------------------
-- Table structure for `article_comment`
-- ----------------------------
DROP TABLE IF EXISTS `article_comment`;
CREATE TABLE `article_comment` (
  `id` varchar(32) NOT NULL,
  `article_id` varchar(32) DEFAULT NULL COMMENT '文章id',
  `user_id` varchar(32) DEFAULT NULL,
  `reply_id` varchar(32) DEFAULT NULL,
  `content` text,
  `time` datetime DEFAULT NULL,
  `r_content` text COMMENT '评论所有的上级ID集合',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章评论表';

-- ----------------------------
-- Records of article_comment
-- ----------------------------
INSERT INTO `article_comment` VALUES ('2', '1', '784972358981328902', null, '12414', '2019-12-24 16:42:26', '4444412124');
INSERT INTO `article_comment` VALUES ('2a516c2605df42d8a39b6ce8f73d837a', '7ce11cc879744c069e3b7b35ea7c34e8', '784972358981328902', '784972358981328902', 'ps  xxxxxx', '2019-12-28 14:21:35', 'dasd1');
INSERT INTO `article_comment` VALUES ('321fae5b89914fbea45aa76f39604ffa', '7ce11cc879744c069e3b7b35ea7c34e8', '784972358981328902', '784972358981328902', 'rqwrqw', '2019-12-28 14:21:50', 'qwrqwr');
INSERT INTO `article_comment` VALUES ('3e0a6a61e89d49f79a581d223082ba3c', '12db592f58364cefabf4989c0e3d2b71', '784972358981328902', '784972358981328902', '帕斯卡拉设计费没空', '2019-12-25 17:37:46', '12412412');
INSERT INTO `article_comment` VALUES ('6fcfbd503cf947ec802eb9550893d980', '50a29866aa9f40139c800a7ab415fe3b', '784972358981328902', null, '12412412', '2019-12-26 16:39:34', null);
INSERT INTO `article_comment` VALUES ('e3c32b8ac3ad4c86a963a019c98b29a0', '12db592f58364cefabf4989c0e3d2b71', '784972358981328902', '784972358981328902', '124124124124124', '2019-12-25 17:37:14', '142124');

-- ----------------------------
-- Table structure for `child_type`
-- ----------------------------
DROP TABLE IF EXISTS `child_type`;
CREATE TABLE `child_type` (
  `id` varchar(32) NOT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `type` int(11) DEFAULT NULL COMMENT '所属的一级分类  ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='二级分类表';

-- ----------------------------
-- Records of child_type
-- ----------------------------
INSERT INTO `child_type` VALUES ('13293cf1d3f44196b0de99ca4a398cc3', '害虫2', '2');
INSERT INTO `child_type` VALUES ('17a094b17cb04fb18a78420560315b3a', '虎皮兰', '1');
INSERT INTO `child_type` VALUES ('4e594184aea04e6280c23d0f519b9f74', '积雪草', '1');
INSERT INTO `child_type` VALUES ('a368c821f3494f2e95bd1cdd9c568528', '苍蝇', '2');
INSERT INTO `child_type` VALUES ('e231e28280a44d0b83ea4d1ed87d2bb8', '苹果', '3');

-- ----------------------------
-- Table structure for `img_ai_distinguish`
-- ----------------------------
DROP TABLE IF EXISTS `img_ai_distinguish`;
CREATE TABLE `img_ai_distinguish` (
  `id` varchar(32) NOT NULL,
  `img_url` varchar(500) NOT NULL COMMENT '图片识别地址',
  `user_id` varchar(32) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '识别类型',
  `result` text COMMENT '识别结果',
  `time` datetime DEFAULT NULL COMMENT '识别时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='百度AI识别结果管理';

-- ----------------------------
-- Records of img_ai_distinguish
-- ----------------------------
INSERT INTO `img_ai_distinguish` VALUES ('2416533f667a42389e17e78417348729', '/img/pic?pictureName=1210807618133245954121010565869188301012100988195688325146.png', '784972358981328902', '2', '{\"log_id\":\"3828081683097964316\",\"result\":[{\"score\":\"0.994255\",\"name\":\"蝗虫\",\"baike_info\":{}},{\"score\":\"0.00108539\",\"name\":\"蝈蝈\",\"baike_info\":{\"baike_url\":\"http://baike.baidu.com/item/%E8%9D%88%E8%9D%88/477\",\"image_url\":\"http://imgsrc.baidu.com/baike/pic/item/9f2f070828381f3064de3b3aa3014c086f06f05a.jpg\",\"description\":\"蝈蝈是昆虫纲，直翅目，螽斯科一些大型鸣虫的通称，个子较大，外形和蝗虫相像，身体草绿色，触角细长。雄虫的前翅互相摩擦，能发出“括括括”的声音，清脆响亮。喜欢吃瓜果、豆类等，人们用小竹笼饲养观赏。作为欣赏娱乐昆虫在中国已有悠久历史，如在古易州(今河北省易县)就有几百年编笼捕蝈蝈的历史。蝈蝈的别名很多，如哥哥、蛞蛞。蝈蝈在中国分布很广，与蟋蟀、油葫芦被称为三大鸣虫。蝈蝈的食物主要是豆类、菜叶、蚂蚱等。公蝈蝈叫声洪亮，深受广大鸣虫爱好者的喜爱。\"}},{\"score\":\"0.000146205\",\"name\":\"长翅稻蝗\",\"baike_info\":{\"baike_url\":\"http://baike.baidu.com/item/%E9%95%BF%E7%BF%85%E7%A8%BB%E8%9D%97/4357704\",\"image_url\":\"http://imgsrc.baidu.com/baike/pic/item/78310a55b319ebc4f962eeb78326cffc1e171681.jpg\",\"description\":\"昆虫名，Oxya velox (Fabricius，1787)，属直翅目蝗总科斑腿蝗科稻蝗属的一种昆虫。分布华北、华中、华东、华南以及广西、四川和西藏等。主要取食中稻叶片等\"}}]}', '2019-12-28 14:20:09');
INSERT INTO `img_ai_distinguish` VALUES ('2d4316f4e82e4b1c8c22a42a325457af', '/img/pic?pictureName=121011084157239296112100981582613135383.png', '784972358981328902', '3', '{\"log_id\":\"4481012747288596282\",\"result\":[{\"score\":0.18692639470100403,\"name\":\"国光苹果\"},{\"score\":0.1459943950176239,\"name\":\"红星苹果\"},{\"score\":0.12378867715597153,\"name\":\"红富士\"}]}', '2019-12-26 16:11:33');
INSERT INTO `img_ai_distinguish` VALUES ('3a9900570fcb468387e9a0ecad682a1a', '/img/pic?pictureName=121011095991887462512100934907810897941.png', '784972358981328902', '1', '{\"log_id\":\"3545929226326921178\",\"result\":[{\"score\":0.5035789012908936,\"name\":\"红马蹄草\",\"baike_info\":{}},{\"score\":0.19812458753585815,\"name\":\"香菇草\",\"baike_info\":{}},{\"score\":0.15643607079982758,\"name\":\"积雪草\",\"baike_info\":{\"baike_url\":\"http://baike.baidu.com/item/%E7%A7%AF%E9%9B%AA%E8%8D%89/1698955\",\"image_url\":\"http://imgsrc.baidu.com/baike/pic/item/77c6a7efce1b9d16a0508ce4fddeb48f8d54649b.jpg\",\"description\":\"积雪草(学名：Centella asiatica (L.) Urban)，为伞形科多年生草本，茎匍匐，细长，节上生根。叶片膜质至草质，圆形、肾形或马蹄形；掌状脉5-7，两面隆起，脉上部分叉；叶柄长1.5-27厘米，无毛或上部有柔毛，基部叶鞘透明，膜质。伞形花序梗2-4个，聚生于叶腋；苞片通常2，很少3，卵形，膜质；每一伞形花序有花3-4，聚集呈头状；花瓣卵形，紫红色或乳白色，膜质；花柱长约0.6毫米；花丝短于花瓣，与花柱等长。果实两侧扁压，圆球形，基部心形至平截形，每侧有纵棱数条，棱间有明显的小横脉，网状，表面有毛或平滑。花果期4-10月。分布于中国多省区。喜生长于阴湿的草地或水沟边；海拔200-1900米。印度、斯里兰卡、马来西亚、印度尼西亚、大洋洲群岛、日本、澳大利亚及中非、南非(阿扎尼亚)也有分布。全草入药，清热利湿、消肿解毒，治痧氙腹痛、暑泻、痢疾、湿热黄疸、砂淋、血淋，吐、血、咳血、目赤、喉肿、风疹、疥癣、疔痈肿毒、跌打损伤等。(概述图片来源：)\"}}]}', '2019-12-26 16:11:52');
INSERT INTO `img_ai_distinguish` VALUES ('544a191a7b7d49df9c389c7088825e82', '/img/pic?pictureName=1210807712110821378121010577831762739312100935771921408014.png', '784972358981328902', '3', '{\"log_id\":\"2554595238556727516\",\"result\":[{\"score\":0.6128817796707153,\"name\":\"红富士\"},{\"score\":0.08879896998405457,\"name\":\"苹果\"},{\"score\":0.0826232060790062,\"name\":\"栖霞苹果\"}]}', '2019-12-28 14:20:33');
INSERT INTO `img_ai_distinguish` VALUES ('56c0f1e204c641cc90cd83d934e2b1c9', '/img/pic?pictureName=121010577831762739312100935771921408014.png', '784972358981328902', '3', '{\"log_id\":\"4724860542009482938\",\"result\":[{\"score\":0.6128817796707153,\"name\":\"红富士\"},{\"score\":0.08879896998405457,\"name\":\"苹果\"},{\"score\":0.0826232060790062,\"name\":\"栖霞苹果\"}]}', '2019-12-26 15:54:53');
INSERT INTO `img_ai_distinguish` VALUES ('58063293ca3f4d4492c7df38f040e746', '/img/pic?pictureName=121010679103232819412100933426298839062.png', '784972358981328902', '3', '{\"log_id\":\"3534802753886192410\",\"result\":[{\"score\":0.9999327659606934,\"name\":\"非果蔬食材\"},{\"score\":0.000025451232431805693,\"name\":\"西瓜\"},{\"score\":0.000013142167517798953,\"name\":\"平菇\"}]}', '2019-12-26 15:55:21');
INSERT INTO `img_ai_distinguish` VALUES ('582daaa6dec5496e9334b9d46a130e8a', '/img/pic?pictureName=121011084157239296112100981582613135383.png', '784972358981328902', '3', '{\"log_id\":\"8369104545854095930\",\"result\":[{\"score\":0.18692639470100403,\"name\":\"国光苹果\"},{\"score\":0.1459943950176239,\"name\":\"红星苹果\"},{\"score\":0.12378867715597153,\"name\":\"红富士\"}]}', '2019-12-26 16:11:31');
INSERT INTO `img_ai_distinguish` VALUES ('6223a52544bc4aac8f681bd4182d0c1b', '/img/pic?pictureName=121011084157239296112100981582613135383.png', '784972358981328902', '3', '{\"log_id\":\"3230190734491281242\",\"result\":[{\"score\":0.18692639470100403,\"name\":\"国光苹果\"},{\"score\":0.1459943950176239,\"name\":\"红星苹果\"},{\"score\":0.12378867715597153,\"name\":\"红富士\"}]}', '2019-12-26 16:11:32');
INSERT INTO `img_ai_distinguish` VALUES ('81c57748e67642baa001f70b942b3d6e', '/img/pic?pictureName=121010577831762739312100935771921408014.png', '784972358981328902', '3', '{\"log_id\":\"828169885640596538\",\"result\":[{\"score\":0.6128817796707153,\"name\":\"红富士\"},{\"score\":0.08879896998405457,\"name\":\"苹果\"},{\"score\":0.0826232060790062,\"name\":\"栖霞苹果\"}]}', '2019-12-26 15:54:55');
INSERT INTO `img_ai_distinguish` VALUES ('8be3eac01b8447fb93ae7517660d877a', '/img/pic?pictureName=121010679103232819412100933426298839062.png', '784972358981328902', '3', '{\"log_id\":\"704706694615710874\",\"result\":[{\"score\":0.9999327659606934,\"name\":\"非果蔬食材\"},{\"score\":0.000025451232431805693,\"name\":\"西瓜\"},{\"score\":0.000013142167517798953,\"name\":\"平菇\"}]}', '2019-12-26 15:55:19');
INSERT INTO `img_ai_distinguish` VALUES ('980dd8fbc23b4c17ba3ca68d167bde39', '/img/pic?pictureName=1210807483156348930121010565869188301012100988195688325146.png', '784972358981328902', '1', '{\"log_id\":\"3116386862548298620\",\"result\":[{\"score\":0.5852232575416565,\"name\":\"非植物\",\"baike_info\":{}}]}', '2019-12-28 14:19:39');
INSERT INTO `img_ai_distinguish` VALUES ('a9fce3f656d84afa93fe373a5215b307', '/img/pic?pictureName=1210110774677438465121010508090553549012100988195688325146.png', '784972358981328902', '2', '{\"log_id\":\"8163461403425943994\",\"result\":[{\"score\":\"0.994255\",\"name\":\"蝗虫\",\"baike_info\":{}},{\"score\":\"0.00108539\",\"name\":\"蝈蝈\",\"baike_info\":{\"baike_url\":\"http://baike.baidu.com/item/%E8%9D%88%E8%9D%88/477\",\"image_url\":\"http://imgsrc.baidu.com/baike/pic/item/9f2f070828381f3064de3b3aa3014c086f06f05a.jpg\",\"description\":\"蝈蝈是昆虫纲，直翅目，螽斯科一些大型鸣虫的通称，个子较大，外形和蝗虫相像，身体草绿色，触角细长。雄虫的前翅互相摩擦，能发出“括括括”的声音，清脆响亮。喜欢吃瓜果、豆类等，人们用小竹笼饲养观赏。作为欣赏娱乐昆虫在中国已有悠久历史，如在古易州(今河北省易县)就有几百年编笼捕蝈蝈的历史。蝈蝈的别名很多，如哥哥、蛞蛞。蝈蝈在中国分布很广，与蟋蟀、油葫芦被称为三大鸣虫。蝈蝈的食物主要是豆类、菜叶、蚂蚱等。公蝈蝈叫声洪亮，深受广大鸣虫爱好者的喜爱。\"}},{\"score\":\"0.000146205\",\"name\":\"长翅稻蝗\",\"baike_info\":{\"baike_url\":\"http://baike.baidu.com/item/%E9%95%BF%E7%BF%85%E7%A8%BB%E8%9D%97/4357704\",\"image_url\":\"http://imgsrc.baidu.com/baike/pic/item/78310a55b319ebc4f962eeb78326cffc1e171681.jpg\",\"description\":\"昆虫名，Oxya velox (Fabricius，1787)，属直翅目蝗总科斑腿蝗科稻蝗属的一种昆虫。分布华北、华中、华东、华南以及广西、四川和西藏等。主要取食中稻叶片等\"}}]}', '2019-12-26 16:11:07');
INSERT INTO `img_ai_distinguish` VALUES ('b90fb21c09234b77a9e670f18b0216a5', '/img/pic?pictureName=121011084157239296112100981582613135383.png', '784972358981328902', '3', '{\"log_id\":\"8889586160184818618\",\"result\":[{\"score\":0.18692639470100403,\"name\":\"国光苹果\"},{\"score\":0.1459943950176239,\"name\":\"红星苹果\"},{\"score\":0.12378867715597153,\"name\":\"红富士\"}]}', '2019-12-26 16:11:30');
INSERT INTO `img_ai_distinguish` VALUES ('bbc0d1fa0c73421bab2355c5e9d1720b', '/img/pic?pictureName=121011095991887462512100934907810897941.png', '784972358981328902', '1', '{\"log_id\":\"1989848872996433178\",\"result\":[{\"score\":0.5035789012908936,\"name\":\"红马蹄草\",\"baike_info\":{}},{\"score\":0.19812458753585815,\"name\":\"香菇草\",\"baike_info\":{}},{\"score\":0.15643607079982758,\"name\":\"积雪草\",\"baike_info\":{\"baike_url\":\"http://baike.baidu.com/item/%E7%A7%AF%E9%9B%AA%E8%8D%89/1698955\",\"image_url\":\"http://imgsrc.baidu.com/baike/pic/item/77c6a7efce1b9d16a0508ce4fddeb48f8d54649b.jpg\",\"description\":\"积雪草(学名：Centella asiatica (L.) Urban)，为伞形科多年生草本，茎匍匐，细长，节上生根。叶片膜质至草质，圆形、肾形或马蹄形；掌状脉5-7，两面隆起，脉上部分叉；叶柄长1.5-27厘米，无毛或上部有柔毛，基部叶鞘透明，膜质。伞形花序梗2-4个，聚生于叶腋；苞片通常2，很少3，卵形，膜质；每一伞形花序有花3-4，聚集呈头状；花瓣卵形，紫红色或乳白色，膜质；花柱长约0.6毫米；花丝短于花瓣，与花柱等长。果实两侧扁压，圆球形，基部心形至平截形，每侧有纵棱数条，棱间有明显的小横脉，网状，表面有毛或平滑。花果期4-10月。分布于中国多省区。喜生长于阴湿的草地或水沟边；海拔200-1900米。印度、斯里兰卡、马来西亚、印度尼西亚、大洋洲群岛、日本、澳大利亚及中非、南非(阿扎尼亚)也有分布。全草入药，清热利湿、消肿解毒，治痧氙腹痛、暑泻、痢疾、湿热黄疸、砂淋、血淋，吐、血、咳血、目赤、喉肿、风疹、疥癣、疔痈肿毒、跌打损伤等。(概述图片来源：)\"}}]}', '2019-12-26 16:11:53');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(32) NOT NULL COMMENT '用户ID',
  `name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `type` int(1) DEFAULT '0' COMMENT '0 未审核  1 已审核  2 已禁用  ',
  `eamil` varchar(20) DEFAULT NULL COMMENT '用户邮箱登录的时候使用',
  `salt` varchar(32) DEFAULT NULL,
  `pwd` varchar(100) DEFAULT NULL COMMENT '用户密码',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录的时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户表';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('47d044dde3404c33ac8c7887bf12e026', '123456', '1', '123456', 'f6a3a889be48403e819634e03e9e668d', '123456', '2019-12-28 14:13:44');
INSERT INTO `user` VALUES ('6ef9ad14c437476ea6b207803c695cfa', '19', '1', 'admin', 'd0dd68f396844433943d9a1fb45ec736', '123456', '2019-12-25 17:16:58');
INSERT INTO `user` VALUES ('784972358981328902', 'Tom', '1', '2', '4', '123456', '2019-12-29 09:33:25');
INSERT INTO `user` VALUES ('784972358981328903', 'Jammy', '1', '3', '5', '123456', '2019-12-23 21:00:00');
INSERT INTO `user` VALUES ('f3453f58455349bb832e82efeb0ef9e3', '693', '1', '369', '185fa564e1504ebba857d70c3d1594dd', '123456', '2019-12-26 17:41:51');

-- ----------------------------
-- Table structure for `user_opinion`
-- ----------------------------
DROP TABLE IF EXISTS `user_opinion`;
CREATE TABLE `user_opinion` (
  `id` varchar(32) NOT NULL,
  `user_id` varchar(32) DEFAULT NULL,
  `content` text COMMENT '内容 ',
  `repaly` text COMMENT '回复',
  `repaly_flag` tinyint(1) DEFAULT NULL COMMENT '是否回复',
  `time` datetime DEFAULT NULL COMMENT '提交时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户意见表';

-- ----------------------------
-- Records of user_opinion
-- ----------------------------
INSERT INTO `user_opinion` VALUES ('0b82345ba1774ecf9a5a19bea6a03a1b', '784972358981328902', '444444', null, null, '2019-12-26 16:29:43');
INSERT INTO `user_opinion` VALUES ('1', '784972358981328902', '按时按时按时按时按时按时按时按时按时按时按时', '124124124', '1', '2019-12-23 17:09:07');
INSERT INTO `user_opinion` VALUES ('6c567357146e4d3cb58418129b200f19', '784972358981328902', '141412', null, null, '2019-12-26 16:29:17');
INSERT INTO `user_opinion` VALUES ('99b6288e641642769f75955f2c9847d5', '784972358981328902', '124124', null, null, '2019-12-26 16:29:23');

-- ----------------------------
-- Table structure for `user_read_article`
-- ----------------------------
DROP TABLE IF EXISTS `user_read_article`;
CREATE TABLE `user_read_article` (
  `id` varchar(32) NOT NULL,
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `article_id` varchar(32) DEFAULT NULL,
  `time` datetime DEFAULT NULL COMMENT '浏览时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户浏览文章表';

-- ----------------------------
-- Records of user_read_article
-- ----------------------------
INSERT INTO `user_read_article` VALUES ('04ed8895d5164918b8e569db00405874', '784972358981328902', '1', '2019-12-28 14:22:38');
INSERT INTO `user_read_article` VALUES ('06fbcc3425254f2488e26c8011a165bf', '6ef9ad14c437476ea6b207803c695cfa', '1', '2019-12-25 17:17:03');
INSERT INTO `user_read_article` VALUES ('146d7f611fe24bbda9d97085e9bd7925', '784972358981328902', '301e46a44a074b9c873f329df2e65b89', '2019-12-25 17:04:58');
INSERT INTO `user_read_article` VALUES ('1d0965100eff49d49fd3b20e4cd0a1ae', '784972358981328902', '12db592f58364cefabf4989c0e3d2b71', '2019-12-26 17:17:18');
INSERT INTO `user_read_article` VALUES ('2a906c6c51ac4ab38635e8153cf02948', '784972358981328902', '3e0a6a61e89d49f79a581d223082ba3c', '2019-12-26 16:44:03');
INSERT INTO `user_read_article` VALUES ('4a0a9b97fa4648af8c81643043a8e540', '784972358981328902', '7ce11cc879744c069e3b7b35ea7c34e8', '2019-12-29 09:35:18');
INSERT INTO `user_read_article` VALUES ('4b93d59f252044039e3fe480dc2c5552', '784972358981328902', '50a29866aa9f40139c800a7ab415fe3b', '2019-12-26 16:39:35');
INSERT INTO `user_read_article` VALUES ('59ae23393ffe424aa590ed0678d6490e', '784972358981328902', '08281edd5b074fa483cff4044d44abc7', '2019-12-25 17:37:16');
INSERT INTO `user_read_article` VALUES ('a233a84fa5f748fab4691d2f978d7663', '784972358981328902', '7b139ca1a4e24d4f90628db3324d189c', '2019-12-26 17:13:59');
