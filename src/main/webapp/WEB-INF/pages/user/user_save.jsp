
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html xmlns:th="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" href="/resources/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/admin.css" media="all">
</head>
<style>
    .layui-form-item{
        margin-bottom: 8px;
    }
</style>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div id="cardBodyDiv" class="layui-card-body" style="padding: 15px;">
            <form class="layui-form layui-form-pane" action="" lay-filter="component-form-group">
                <input type="hidden" name="id" value="${vo.id}"/>
                <div class="layui-form-item">
                    <label class="layui-form-label">用户名</label>
                    <div class="layui-input-block">
                        <input id="loginName" type="text" name="name"
                               value="${vo.name}" lay-verify="title" maxlength="20" autocomplete="off" placeholder="请输入用户名登录名" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">登录邮箱</label>
                    <div class="layui-input-block">
                        <input id="text" type="text" name="eamil" value="${vo.eamil}" maxlength="18" autocomplete="off" placeholder="请输入用户登录名" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">状态</label>
                    <div class="layui-input-block">
                        <select name="type" class="selectedlv" v="${vo.type}" >
                            <option value="0">未审核</option>
                            <option value="1">审核通过</option>
                            <option value="2">审核拒绝</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <a class="layui-btn" lay-submit="" lay-filter="component-form-demo1">保存</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="/resources/layuiadmin/layui/layui.js"></script>
<script src="/resources/jquery-2.2.4.js"></script>
<script src="/resources/base.js"></script>

<script>
    selected();
    layui.config({
        base: '/resources/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,element = layui.element
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,form = layui.form;
        form.render(null, 'component-form-group');
        laydate.render({
            elem: '#LAY-component-form-group-date'
        });
        /* 监听提交 */
        form.on('submit(component-form-demo1)', function(data){
            var da = data.field;
            layer.confirm('确定保存用户信息？', {
                btn: ['确定','取消'], //按钮
                skin: 'layui-layer-molv'
            }, function(){
                basePost({
                    url:"/user/save.htm",
                    data:da,
                    success:function (req) {
                        layer.confirm('用户保存成功！', {
                            title: '提示',
                            btn: ['确定'], //按钮
                            skin: 'layui-layer-molv',
                            closeBtn: 0
                        }, function(){
                            window.parent.layer.closeAll();
                            // 修改了自己的密码，应该重新登录
                            window.parent.location.href="/user/indexPage.htm";
                        });
                    }
                })
            }, function(){
            });
            return false;
        });
    });

</script>

</body>

</html>

