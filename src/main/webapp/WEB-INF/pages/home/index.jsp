<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>农技交流中心</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link href="https://cdn.bootcss.com/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/resources/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/template.css" media="all">
</head>
<body class="layui-layout-body" >
<div class="layui-carousel" id="test1" >
    <div carousel-item="" >
        <c:forEach items="${topArticle}" var="article">
            <div class="layui-fluid layadmin-maillist-fluid" style="margin-top: 50px;height: 30px;">
                <div class="layui-row layui-col-space15">
                    <div style="text-align: center;">
                        <div class="layui-col-md12 layui-col-sm6">
                            <div class="layadmin-contact-box" >
                                <div class="layui-col-md4 layui-col-sm6">
                                    <a href="javascript:;">
                                        <div class="layadmin-text-center">
                                            <img src="${article.cover}" style="height: 140px;">
                                        </div>
                                    </a>
                                </div>
                                <div class="layui-col-md8 layadmin-padding-left20 layui-col-sm6">
                                    <a href="javascript:;">
                                        <h3 class="layadmin-title" onclick="yuedu('${article.id}')">
                                            <strong>${article.title}</strong>
                                        </h3>
                                        <p class="layadmin-textimg">
                                            用户: ${article.userName}
                                            <br>时间: ${article.time}
                                        </p>
                                    </a>
                                    <div class="layadmin-address">
                                        <a href="javascript:;">
                                            摘要:${article.summary}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<img style="height: 240px;" src="http://localhost:8080/img/pic?pictureName=12093904028060835858ZWZA4F%5DE%40DQ%7BEUQKII4H%601.png">--%>

            </div>
        </c:forEach>
    </div>
</div>

<!--  头部-->
<div id="LAY_app">
    <div class="layui-layout layui-layout-admin">
        <div class="layui-header">
            <!-- 头部区域 -->
            <ul class="layui-nav layui-layout-left">
                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                </li>
            </ul>
            <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">
                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="fullscreen">
                        <i class="layui-icon layui-icon-screen-full"></i>
                    </a>
                </li>
                <li class="layui-nav-item" lay-unselect style="margin-right: 50px;">
                    <a href="javascript:;">
                        <cite>
                            <c:if test="${loginFlag== false}">请登录/注册</c:if>
                            <c:if test="${loginFlag== true}">${userEntity.name}</c:if>
                        </cite>
                    </a>
                    <dl class="layui-nav-child">
                        <c:if test="${loginFlag== false}">
                            <dd  style="text-align: center;" onclick="login()"><a>登录</a></dd>
                            <dd  style="text-align: center;" onclick="reg()"><a>注册</a></dd>
                        </c:if>
                        <c:if test="${loginFlag== true}">
                            <dd  style="text-align: center;"><a href="/userInfo/info.htmx">个人中心</a></dd>
                            <dd  style="text-align: center;" onclick="fabiao()"><a>发表文章</a></dd>
                            <dd style="text-align: center;"><a href="/logOut.htmx">退出</a></dd>
                        </c:if>
                        <dd  style="text-align: center;" onclick="sunmitStu()"><a>农技学习</a></dd>
                    </dl>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--一份分类-->
<div class="layui-col-md12 layui-col-sm12" style="margin-top: 5px;background-color: white;padding: 10px;">
    <div class="layui-col-md12 layui-col-sm12">
        <input type="button" class="layui-btn types layui-btn-xs layui-btn-primary layui-btn-disabled" onclick="typek('all',this,null)"  style="margin-left: 20px;" value="首页(${mnap.all})">
        <input type="button" class="layui-btn types layui-btn-xs " style="margin-left: 20px;"  onclick="typek('zhizhu',this,1)"  value="植株(${mnap.zhizhu})">
        <input type="button" class="layui-btn types layui-btn-xs layui-btn-warm" style="margin-left: 20px;" onclick="typek('haichong',this,2)"  value="害虫(${mnap.haichong})">
        <input type="button" class="layui-btn types layui-btn-xs" style="margin-left: 20px;" onclick="typek('guoshu',this,3)"  value="果蔬(${mnap.guoshu})">
    </div>
    <div class="layui-col-md12 layui-col-sm12 types3" id="all" style="margin-top: 5px;padding: 10px;">
        <c:forEach items="${map.all}" var="s">
            <span style="margin-left: 5px;color: #1E9FFF;cursor:pointer" class="poiop" onclick="labelf('${s}',this)">#${s}</span>
        </c:forEach>
    </div>
    <div class="layui-col-md12 layui-col-sm12 types3" id="zhizhu" style="margin-top: 5px;padding: 10px;display: none;">
        <c:forEach items="${map.zhizhu}" var="s">
            <span style="margin-left: 5px;color: #1E9FFF;cursor:pointer" class="poiop" onclick="labelf('${s}',this)">#${s}</span>
        </c:forEach>
    </div>

    <div class="layui-col-md12 layui-col-sm12 types3" id="haichong" style="margin-top: 5px;padding: 10px;display: none;">
        <c:forEach items="${map.haichong}" var="s">
            <span style="margin-left: 5px;color: #1E9FFF;cursor:pointer" class="poiop" onclick="labelf('${s}',this)">#${s}</span>
        </c:forEach>
    </div>

    <div class="layui-col-md12 layui-col-sm12 types3" id="guoshu" style="margin-top: 5px;padding: 10px;display: none;">
        <c:forEach items="${map.guoshu}" var="s">
            <span style="margin-left: 5px;color: #1E9FFF;cursor:pointer" class="poiop" onclick="labelf('${s}',this)">#${s}</span>
        </c:forEach>
    </div>
</div>
<div class="layui-col-md12 layui-col-sm12" style="margin-top: 5px;background-color: white;padding: 10px;height: 300px;overflow: auto;">
    <!-- 文章列表-->
    <form id="userForm" class="layui-form-pane">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">标题</label>
                <div class="layui-input-block">
                    <input class="layui-input" name="title" id="title"   autocomplete="off" >
                </div>
            </div>
            <div class="layui-inline"style="padding-right: 7%">
                <a class="layui-btn" onclick="sunsec()" ><i class="layui-icon">&#xe615;</i>搜索</a>
            </div>
        </div>
    </form>
    <table class="layui-hide" id="test-table-checkbox" ></table>
</div>

<script src="/resources/jquery-2.2.4.js"></script>
<script src="/resources/layuiadmin/layui/layui.js"></script>
<script src="/resources/base.js"></script>
<script>
    try {
        localStorage.setItem("layuiAdmin", '{"theme":{"color":{"logo":"#226A62","header":"#2F9688","alias":"green-header","index":9}},"note":""}');
    } catch (e) {
    }

    layui.config({
        base: '/resources/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','table','carousel'],function () {
        var carousel = layui.carousel;
        //建造实例
        carousel.render({
            elem: '#test1'
            ,width: '100%' //设置容器宽度
            ,arrow: 'always'//始终显示箭头
            ,height: '240px'
            //,anim: 'updown' //切换动画方式
        });


        var table = layui.table;
        // 定义数据
        var data = [
            {field: 'cover', width: 200, title: '封面', align: "center",templet: function (d){
                return "<img src='"+d.cover+"'/>"
            }}
            ,{field: 'title', width: 400, title: '标题', align: "center",templet: function (d){
                return "<a style='color: #1E9FFF;cursor:pointer'  onclick='yuedu(\""+d.id+"\")' >"+d.title+"</a>"
                }}
            ,{field: 'summary', width: 600, title: '摘要', align: "center"}
            ,{field: 'label', width: 200, title: '标签', align: "center"}
            ,{field: 'time', width: 170, title: '发布时间', align: "center"}
            , {
                field: 'type', title: '一级分类', width: 100,  align: "center", templet: function (d) {
                    if (d.type == 1) {
                        return '植被'
                    }
                    else if (d.type == 2) {
                        return '害虫'
                    }
                    else if (d.type == 3) {
                        return '果蔬'
                    }
                }
            }
            , {
                field: 'childName', title: '二级分类', width: 100,  align: "center"
            }
            , {
                field: 'userName', title: '用户', width: 120,  align: "center", templet: function (d) {
                    if (d.userName == null || d.userName=="") {
                        return '<span style="color: red">系统发布</span>'
                    }
                    else {
                        return d.userName;
                    }
                }
            }
        ];
        // 渲染表格数据
        table.render({
            elem: '#test-table-checkbox' // 表格id
            , url: '/article/page.htmx' // 数据接口（用于获取数据）
            , cols: [data] // 表格数据
            , page: true // 开启分页
            , limit: 10 // 每页数据条数
            , limits: [10, 30, 50, 100] // 设置可供选择的每页数据量
            , even: true
            , id: "user_list"
             ,height:500
            , done: function () {
            }
        });
    });
    var data = {
        type: null,
        label: null
    };


    function tableReload() {

        layui.table.reload('user_list', {
            page: {
                curr: 1 //重新从第 1 页开始
            }
            , where: data
        });
    }

    function labelf(label,b) {
        data.label = label;
        $(".poiop").css("color","#1E9FFF");
        $(b).css("color","red");
        tableReload();
    }


    function sunsec(){
        data.title = $("#title").val();
        tableReload();
    }


    function typek(id,b,type) {
        data.type = type;
        $(".types").removeClass("layui-btn-disabled");
        $(b).addClass("layui-btn-disabled");
        $(".types3").hide();
        $("#"+id).show();
        tableReload();
    }

    function yuedu(id) {
        //iframe层
        layui.layer.open({
            type: 2,
            title: '文章阅读',
            shadeClose: true,
            shade: 0.8,
            area: ['90%', '90%'],
            content: '/article/info.htmx?id='+id //iframe的url
        });
    }
    
    function login() {
        layui.layer.open({
            type: 2,
            title: '用户登录',
            shadeClose: true,
            shade: 0.8,
            area: ['600px', '445px'],
            content: '/login.htmx' //iframe的url
        });
    }

    function reg() {
        layui.layer.open({
            type: 2,
            title: '用户注册',
            shadeClose: true,
            shade: 0.8,
            area: ['600px', '445px'],
            content: '/reg.htmx' //iframe的url
        });
    }

    function sunmitStu(){
        window.open("/study.htmx");
    }

    
    function fabiao() {
        layui.layer.open({
            type: 2,
            title: '发表文章',
            shadeClose: true,
            shade: 0.8,
            area: ['90%', '90%'],
            content: '/article/add.htmx' //iframe的url
        });
    }

</script>
</body>
</html>
