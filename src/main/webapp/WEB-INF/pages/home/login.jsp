
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>植株管理系统后台登录</title>
    <link rel="shortcut icon" href="/admin/icon/fa.png" type="image/x-icon"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/resources/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/login.css" media="all">
</head>
<body style="background-image: url(/resources/bg.jpg);height: 400px;">

<div class="layadmin-user-login layadmin-user-display-show" id="LAY-user-login" style="display: none;height: 300px;">
    <div class="layadmin-user-login-main" style="background-color:rgba(255,255,255,0.1);margin-top: 3%;">
        <div class="layadmin-user-login-box layadmin-user-login-header">
            <h2>植株检测系统</h2>
        </div>
        <div class="layadmin-user-login-box layadmin-user-login-body layui-form">
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-username"
                       for="LAY-user-login-username"></label>
                <input type="text" name="username" id="LAY-user-login-username" lay-verify="required"
                       placeholder="用户邮箱" class="layui-input" onKeyPress="jumpByEnter($('#LAY-user-login-password'))">
            </div>
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-password"
                       for="LAY-user-login-password"></label>
                <input type="password" name="password" id="LAY-user-login-password" lay-verify="required"
                       placeholder="密码" class="layui-input" onKeyPress="jumpByEnter($('#LAY-user-login-vercode'))">
            </div>
            <div class="layui-form-item">
                <button class="layui-btn layui-btn-fluid" id="loginBtn" onclick="sub()">登 入</button>
            </div>
        </div>
    </div>
</div>
<script src="/resources/jquery-2.2.4.js"></script>
<script src="/resources/layuiadmin/layui/layui.js"></script>
<script src="/resources/base.js"></script>
<script>

    $(function () {
        // 进入页面直接聚焦到用户名输入框
        $('#LAY-user-login-username').focus();
    });

    // 聚焦到指定元素
    function jumpByEnter(nextEle) {
        var curKeyCode = (navigator.appname === "Netscape") ? event.which : event.keyCode; //event.keyCode按的建的代码，13表示回车
        if (curKeyCode === 13) {
            nextEle.focus();
        }
    }

    // 回车登录
    function enterKeyLogin() {
        var curKeyCode = (navigator.appname === "Netscape") ? event.which : event.keyCode;
        if (curKeyCode === 13) {
            $('#loginBtn').click();
        } else
            return false;
    }


    function sub() {
        var userName = $("#LAY-user-login-username").val();
        var passWord = $("#LAY-user-login-password").val();
        var code = $("#LAY-user-login-vercode").val();
        var data = {userName: userName, passWord: passWord, code: code};
        basePost({
            url: "/loginData.htmx",
            data: data,
            success: function (req) {
                window.parent.location.href = "/"
            },
            error: function (req) {
                layui.layer.alert("服务器异常:" + req.msg, {icon: 2, skin: 'layui-layer-molv'});
            }
        })
    }


    try {
        localStorage.setItem("layuiAdmin", '{"theme":{"color":{"logo":"#226A62","header":"#2F9688","alias":"green-header","index":9}},"note":""}');
    } catch (e) {
    }

    layui.config({
        base: '/resources/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index'], function () {

    });
</script>
</body>
</html>