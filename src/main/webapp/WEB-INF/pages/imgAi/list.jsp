<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/resources/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/admin.css" media="all">
</head>
<body>


<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">
                    <table class="layui-hide" id="test-table-checkbox" ></table>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="/resources/layuiadmin/layui/layui.js"></script>
<script src="/resources/jquery-2.2.4.js"></script>
<script src="/resources/base.js"></script>
<script>

    layui.config({
        base: '/resources/layuiadmin/' //静态资源(layui.js)所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'table','layer'], function () {
        var table = layui.table;
        // 定义数据
        var data = [
            {field: 'time', width: 180, title: '时间', align: "center"},
            {field: 'userName', width: 120, title: '用户', align: "center"} ,
            {field: 'img_url', width: 120, title: '原图', align: "center", templet: function (d){
                return '<img style="width：100px;height:50px;" src="'+d.imgUrl+'" />';
            }},
            {field: '3', width: 220, title: '识别名称', align: "center", templet: function (d){
                var result = JSON.parse(d.result);
                if(result.result){
                    return (result.result)[0].name;
                }
            }},
            {field: '4', minwidth: 320, title: '识别结果', align: "center", templet: function (d){
                return '<span class="layui-btn layui-btn-xs" oid="'+d.id+'" onclick="imgInfo(this)">识别结果</span>'
            }},
            {field: '4', width: 220, title: '识别结果', align: "center", templet: function (d){
                return '<span class="layui-btn layui-btn-xs layui-btn-warm" oid="'+d.id+'" onclick="deleteA(this)">删除识别</span>'
            }},
        ];
        // 渲染表格数据
        table.render({
            elem: '#test-table-checkbox' // 表格id
            , url: '/imgAiDistinguish/page.htm?type=${type}' // 数据接口（用于获取数据）
            , cols: [data] // 表格数据
            , page: true // 开启分页
            , limit: 10 // 每页数据条数
            , limits: [10, 30, 50, 100] // 设置可供选择的每页数据量
            , even: true
            , id: "user_list"
            , done: function () {

            }
        });
    });

    function edit(b) {
        layui.layer.open({
            type: 2,
            title: '添加二级分类',
            area: ['600px', "400px"],
            skin: 'layui-layer-molv',
            content: "/childType/savePage.htm",
        });
    }

    function imgInfo(b) {
        var id = $(b).attr("oid");
        layui.layer.open({
            type: 2,
            title: '识别详情',
            shadeClose: true,
            shade: 0.8,
            area: ['900px', '600px'],
            content: '/userInfo/imgInfo.htmx?id='+id //iframe的url
        });
    }


    function deleteA(b){
        var id = $(b).attr("oid");
        layui.layer.confirm('确定删除信息？', {
            btn: ['确定','取消'], //按钮
            skin: 'layui-layer-molv'
        }, function(){
            basePost({
                url:"/imgAiDistinguish/delete.htmx",
                data:{
                    id:id
                },
                success:function (req) {
                    layer.confirm('删除成功', {
                        title: '提示',
                        btn: ['确定'], //按钮
                        skin: 'layui-layer-molv',
                        closeBtn: 0
                    }, function(){
                        window.location.reload()
                    });
                }
            })
        }, function(){
        });
    }

</script>
</body>
</html>
