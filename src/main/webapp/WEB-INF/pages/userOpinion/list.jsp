<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/resources/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/admin.css" media="all">
</head>
<body>


<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">
                    <div class="demoTable">
                        <form id="userForm" class="layui-form-pane">
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">内容</label>
                                    <div class="layui-input-block">
                                        <input class="layui-input" name="content"  autocomplete="off" placeholder="请输入">
                                    </div>
                                </div>
                                <div class="layui-inline"style="float: right; padding-right: 7%">
                                    <a class="layui-btn" id="selectBtn"><i class="layui-icon">&#xe615;</i>搜索</a>
<%--                                    <a  style="margin-left: 80px;" onclick="edit(this)" class="layui-btn" id="addUserBtn"> <i class="layui-icon">&#xe654;</i>添加用户</a>--%>
                                </div>
                            </div>

                        </form>
                    </div>
                    <table class="layui-hide" id="test-table-checkbox" ></table>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="/resources/layuiadmin/layui/layui.js"></script>
<script src="/resources/jquery-2.2.4.js"></script>
<script src="/resources/base.js"></script>
<script>

    layui.config({
        base: '/resources/layuiadmin/' //静态资源(layui.js)所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'table','layer'], function () {
        var table = layui.table;
        // 定义数据
        var data = [
            {field: 'userName', width: 200, title: '用户', align: "center"},
            {field: 'content', width: 500, title: '意见', align: "center"}
            , {field: 'repaly', width: 500, title: '回复',  align: "center"}
            , {
                field: 'repalyFlag', title: '是否回复', width: 100,  align: "center", templet: function (d) {
                    if (d.repalyFlag == 0) {
                        return '<span style="color: #b8b8b8;">未回复</span>'
                    }
                    else if (d.repalyFlag == 1) {
                        return '<span style="color: #0ccc12;">已回复</span>'
                    }
                }
            }
            , {field: 'time', width: 180, title: '时间',  align: "center"}
            , {
                fixed: 'right', minWidth: 105, align: 'center', title: '操作', templet: function (d) {
                    if(d.repalyFlag == 0){
                        var html = '<div class="layui-btn-group">\n' +
                            '  <button  class="layui-btn layui-btn-sm" bid="'+d.id+'" onclick="edit(this)">\n' +
                            '    <i class="layui-icon">&#xe642;</i>回复' +
                            '  </button>\n'
                        return  html+ '</div>';
                    }
                    return "";
                }
            }
        ];
        // 渲染表格数据
        table.render({
            elem: '#test-table-checkbox' // 表格id
            , url: '/userOpinion/page.htm' // 数据接口（用于获取数据）
            , cols: [data] // 表格数据
            , page: true // 开启分页
            , limit: 10 // 每页数据条数
            , limits: [10, 30, 50, 100] // 设置可供选择的每页数据量
            , even: true
            , id: "user_list"
            , done: function () {

            }
        });
        var $ = layui.$;
        $("#selectBtn").on('click', function () {
            tableReload();
        });
    });

    function tableReload() {
        var data = getFormData("userForm");
        layui.table.reload('user_list', {
            page: {
                curr: 1 //重新从第 1 页开始
            }
            , where: data
        });
    }


    
    function edit(b) {
        var id = $(b).attr("bid");
        layer.prompt({title: '回复用户', formType: 2}, function(text, index){
            basePost({
                url:"/userOpinion/save.htm",
                data:{
                    id:id,
                    repalyFlag:1,
                    repaly:text
                },
                success:function (req) {
                    layer.confirm('回复成功！', {
                        title: '提示',
                        btn: ['确定'], //按钮
                        skin: 'layui-layer-molv',
                        closeBtn: 0
                    }, function(){
                        window.layer.closeAll();
                        // 修改了自己的密码，应该重新登录
                        window.location.href="/userOpinion/indexPage.htm";
                    });
                }
            })
        });
    }

</script>
</body>
</html>
