<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/resources/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/template.css" media="all">
</head>
<body>
<div class="layui-fluid layadmin-homepage-fluid">
    <div class="layui-row layui-col-space8">
        <div class="layui-col-md2">
            <div class="layadmin-homepage-panel layadmin-homepage-shadow">
                <div class="layui-card text-center">
                    <div class="layui-card-body">
                        <h4 class="layadmin-homepage-font">${user.name}</h4>
                        <p class="layadmin-homepage-min-font">${user.eamil}</p>
                        <a class="layui-btn layui-btn-fluid" href="/">回到主页</a>
                    </div>
                </div>
                <div class="layui-card homepage-bottom">
                    <div class="layui-card-header">
                        <h3 class="panel-title">
                            <i class="layui-icon layui-icon-user"></i>&nbsp;&nbsp;&nbsp;AI识别
                        </h3>
                    </div>
                    <div class="layui-card-body">
                        <a href="javascript:;" class="layadmin-privateletterlist-item">
                            <div class="meida-right">
                                <mdall class="layui-btn" onclick="aiImg(1)">植株识别</mdall>
                            </div>
                        </a>
                    </div>
                    <div class="layui-card-body">
                        <a href="javascript:;" class="layadmin-privateletterlist-item">
                            <div class="meida-right">
                                <mdall class="layui-btn layui-btn-warm" onclick="aiImg(2)">害虫识别</mdall>
                            </div>
                        </a>
                    </div>
                    <div class="layui-card-body">
                        <a href="javascript:;" class="layadmin-privateletterlist-item">
                            <div class="meida-right">
                                <mdall class="layui-btn" onclick="aiImg(3)">果蔬识别</mdall>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="layui-card homepage-bottom">
                    <div class="layui-card-header">
                        <h3 class="panel-title">
                            <i class="layui-icon layui-icon-user"></i>&nbsp;&nbsp;&nbsp;<span class="layui-btn layui-btn-xs layui-btn-warm" onclick="subyj()">提交建议</span>我的意见
                        </h3>
                    </div>
                    <div class="layui-card-body">
                        <a href="javascript:;" class="layadmin-privateletterlist-item">
                            <div class="meida-right">
                                <c:forEach items="${userOpinionEntities}" var="xinion">
                                    <mdall>
                                        意见:${xinion.content}
                                        <c:if test="${xinion.repaly !=null }">
                                            <br>
                                            <span style="color: #0066cc;"> 回复:${xinion.repaly}</span>
                                        </c:if>

                                    </mdall>
                                    <hr>
                                </c:forEach>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-col-md10">
            <div class="layui-fluid layadmin-homepage-content">
                <div class="layui-row layui-col-space20 layadmin-homepage-list-imgtxt">
                    <div class="layui-col-md9">
                        <div class="layui-card-header " style="background-color: white">
                            <h3 class="panel-title">
                                <i class="layui-icon layui-icon-user"></i>&nbsp;&nbsp;&nbsp;我的文章 <span class="layui-btn layui-btn-xs" style="margin-left: 50px;" onclick="fabiao()">发表文章</span>
                            </h3>
                        </div>
                        <div style="height: 5px;"></div>
                        <c:forEach items="${articleEntityist}" var="article">
                            <div class="grid-demo">
                                <div class="panel-body layadmin-homepage-shadow">
                                    <a href="javascript:;" class="media-left">
                                        <img src="${article.cover}" height="46px" width="46px">
                                    </a>
                                    <div class="media-body">
                                        <div class="pad-btm">
                                            <p class="fontColor"><a href="/article/info.htmx?id=${article.id}">${article.title}</a>          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- 发表于:${article.time}</p>
                                        </div>
                                        <p><b>${article.summary}</b></p>
                                        <div class="media-list">
                                           <span class="layui-btn layui-btn-xs layui-btn-warm" onclick="delA('${article.id}')">删除</span>

                                            <span class="layui-btn layui-btn-xs" onclick="editA('${article.id}')">编辑</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <div class="layui-col-md3">
                        <div class="grid-demo">
                            <div class="layui-card homepage-top">
                                <div class="layui-card-header">
                                    <h3 class="panel-title">
                                        <i class="layui-icon layui-icon-picture"></i>&nbsp;&nbsp;我的识别
                                    </h3>
                                </div>
                                <div class="layui-card-body">
                                    <div class="layui-row layui-col-space15" style="max-height: 400px;overflow: auto">
                                        <c:forEach items="${imgAiDistinguishEntities}" var="imgd">
                                            <div class="layui-col-md4" onclick="imgInfo('${imgd.id}')">
                                                <img src="${imgd.imgUrl}" style="height: 100px;width: 100px;" >
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-card homepage-bottom">
                                <div class="layui-card-header">
                                    <h3 class="panel-title">
                                        <i class="layui-icon layui-icon-user"></i>&nbsp;&nbsp;&nbsp;我的足迹
                                    </h3>
                                </div>
                                <div class="layui-card-body">
                                    <c:forEach items="${zujis}" var="zuji">
                                        <a href="javascript:;" class="layadmin-privateletterlist-item">
                                            <div class="meida-left">
                                                <img src="${zuji.cover}">
                                            </div>
                                            <div class="meida-right">
                                                <p>${zuji.title}</p>
                                                <mdall>${zuji.summary}</mdall>
                                            </div>
                                        </a>
                                    </c:forEach>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/resources/jquery-2.2.4.js"></script>
<script src="/resources/layuiadmin/layui/layui.js"></script>
<script src="/resources/base.js"></script>
<script>
    layui.config({
        base: '/resources/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index'],function () {
        
    });

    function fabiao() {
        layui.layer.open({
            type: 2,
            title: '发表文章',
            shadeClose: true,
            shade: 0.8,
            area: ['90%', '90%'],
            content: '/article/add.htmx' //iframe的url
        });
    }
    function aiImg(vv) {
        layui.layer.open({
            type: 2,
            title: '图片识别',
            shadeClose: true,
            shade: 0.8,
            area: ['900px', '600px'],
            content: '/userInfo/aiImg.htmx?type='+vv //iframe的url
        });
    }

    function imgInfo(id) {
        layui.layer.open({
            type: 2,
            title: '识别详情',
            shadeClose: true,
            shade: 0.8,
            area: ['900px', '600px'],
            content: '/userInfo/imgInfo.htmx?id='+id //iframe的url
        });
    }
    
    function delA(id) {
        layui.layer.confirm('确定删除您的文章？', {
            btn: ['确定','取消'], //按钮
            skin: 'layui-layer-molv'
        }, function(){
            basePost({
                url:"/article/delA.htmx",
                data:{
                    id:id
                },
                success:function (req) {
                    layer.confirm('删除成功', {
                        title: '提示',
                        btn: ['确定'], //按钮
                        skin: 'layui-layer-molv',
                        closeBtn: 0
                    }, function(){
                        window.location.href="/userInfo/info.htmx";
                    });
                }
            })
        }, function() {
        });
    }
    
    function subyj() {
        layer.prompt({title: '请填写你对本系统的意见与建议', formType: 2}, function(text, index){
            basePost({
                url: "/userInfo/userOpinion.htmx",
                data:{text:text},
                success:function (data) {
                    layer.confirm('感谢您的反馈！', {
                        title: '提示',
                        btn: ['确定'], //按钮
                        skin: 'layui-layer-molv',
                        closeBtn: 0
                    }, function(){
                        window.layer.closeAll();
                        window.location.href="/userInfo/info.htmx";
                    });
                }
            })
        });
    }

    function editA(id){
        layui.layer.open({
            type: 2,
            title: '编辑文章',
            area: ['90%', "600px"],
            skin: 'layui-layer-molv',
            content: "/article/edit.htm?id="+id,
        });
    }

</script>

</body>

</html>
