<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/resources/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/admin.css" media="all">
</head>
<body>
    <div class="layui-col-md3">
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-sm4">
                    <div class="layui-card">
                        <div class="layui-card-body" style="height: 500px;">
                            <div class="layui-upload-drag" id="test-upload-drag" style="width: 200px;height: 200px;">
                                <i class="layui-icon"></i>
                                <p>点击上传，或将文件拖拽到此处</p>
<%--                                <img id='aiImg' src='/img/pic?pictureName=12101007849819381772.png' style='width: 200px;height: 200px;'/>--%>
                            </div>
                            <hr>
                            <span class="layui-btn" style="margin-left:25px;margin-top: 90px;width: 200px;" onclick="shibie()">识别</span>
                        </div>
                    </div>
                </div>
                <div class="layui-col-sm8">
                    <div class="layui-card" style="height: 520px;">
                        <div class="layui-card-body" >
                            <p>识别结果</p>
                            <hr>
                            <div>
                                <table class="layui-table">
                                    <tr>
                                        <th>名称</th>
                                        <th>相似度</th>
                                        <th>百科图片</th>
                                        <th>百度百科</th>
                                    </tr>
                                    <tbody  id = "idp">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="/resources/jquery-2.2.4.js"></script>
<script src="/resources/layuiadmin/layui/layui.js"></script>
<script src="/resources/base.js"></script>

<script>
    layui.config({
        base: '/resources/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'upload'], function(){
        var $ = layui.jquery
            ,upload = layui.upload;
        //拖拽上传
        upload.render({
            elem: '#test-upload-drag'
            ,url: '/img/upload.htm'
            ,done: function(res){
                if(res.code==200){
                    $("#test-upload-drag").html("<img id='aiImg' src='"+res.data+"' style='width: 200px;height: 200px;'/>")
                    $("#idp").html("")
                }
                console.log(res)
            }
        });
    });
    
    function shibie() {
        var type = '${type}';
        basePost({
            url:"/userInfo/aiImgData.htmx",
            data:{
                type:type,
                fileName:$("#aiImg").attr("src").replace("/img/pic?pictureName=",""),
            },
            success:function (data) {
                $("#idp").html("")
                if(data){
                    var result = data.result;
                    if(result){
                        for (let i = 0; i < result.length; i++) {
                            var ht = "<tr> ";
                            var red = result[i];
                            var name = red.name;
                            var score = (red.score*100).toFixed(2)+'%';
                            ht+="<td>"+name+"</td>";
                            ht+="<td>"+score+"</td>";
                            var baike = red.baike_info;
                            if(baike){
                                var img = baike.image_url;
                                var baike = baike.baike_url;
                                if(img){
                                    ht+="<td><img src='"+img+"' alt='' style='width: 50px;height: 50px;'></td>";
                                }else{
                                    ht+="<td>无</td>";
                                }
                                if(baike){
                                    ht+="<td><a style='color: #1E9FFF;' onclick='baike(this)'  khref='"+baike+"'>百度百科</a></td>";
                                }else{
                                    ht+="<td>无</td>";
                                }
                            }else{
                                ht+="<td colspan='2'>无百科信息</td>";
                            }
                            ht+= "</tr> ";
                            $("#idp").append(ht);
                        }
                    }else{
                        $("#idp").append('<tr><td colspan="4">暂无识别信息</td></tr>')
                    }
                }else{
                    $("#idp").append('<tr><td colspan="4">暂无识别信息</td></tr>')
                }

                console.log(data);
            }
        })

    }


    function baike(b) {
        var href = $(b).attr("khref");
        window.top.open(href,"_blank");
    }
</script>

</body>

</html>