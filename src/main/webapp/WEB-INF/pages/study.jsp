<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html><head>
    <meta charset="utf-8">
    <title>农技学习</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/resources/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/admin.css" media="all">
    <link href="https://cdn.bootcss.com/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link id="layuicss-layer" rel="stylesheet" href="https://www.layui.com/admin/std/dist/layuiadmin/layui/css/modules/layer/default/layer.css?v=3.1.1" media="all"></head>
<body layadmin-themealias="default"  style="background-color: white;">
<div class="layui-fluid" >
    <div class="layui-row layui-col-space10">
        <div class="layui-col-xs3">
            <!-- 填充内容 -->
            <div class="layui-card">
                <div class="layui-card-body layui-btn layui-btn-primary layui-col-xs12 layui-btn-disabled  showType" onclick="showType(0,this)" style="height: 40px;"><i class="layui-icon layui-icon-find-fill" ></i>全部</div>
            </div>
        </div>
        <div class="layui-col-xs3">
            <!-- 填充内容 -->
            <div class="layui-card">
                <div class="layui-card-body layui-btn layui-col-xs12 showType" onclick="showType(1,this)" style="height: 40px;"><i class="layui-icon layui-icon-find-fill" ></i>植株</div>
            </div>
        </div>
        <div class="layui-col-xs3">
            <!-- 填充内容 -->
            <div class="layui-card">
                <div class="layui-card-body layui-btn layui-btn-warm layui-col-xs12 showType" onclick="showType(2,this)" style="height: 40px;"><i class="layui-icon layui-icon-find-fill" ></i>害虫</div>
            </div>
        </div>
        <div class="layui-col-xs3">
            <!-- 填充内容 -->
            <div class="layui-card">
                <div class="layui-card-body layui-btn layui-col-xs12 showType" onclick="showType(3,this)" style="height: 40px;"><i class="layui-icon layui-icon-find-fill" ></i>果蔬</div>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="layui-fluid" >
    <div class="layui-row layui-col-space10">
        <c:forEach items="${childTypes}" var="childType">
            <div class="layui-col-xs3 childType child${childType.type}" >
                <!-- 填充内容 -->
                <div class="layui-card">
                    <div class="layui-card-body layui-btn layui-btn-primary layui-col-xs12 cccc" onclick="getData('${childType.id}',this)" style="height: 40px;">${childType.name}</div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
<hr>
<div class="layui-fluid" >
    <div class="layui-row layui-col-space10" id="poi">
    </div>
</div>


<script src="/resources/jquery-2.2.4.js"></script>
<script src="/resources/layuiadmin/layui/layui.js"></script>
<script src="/resources/base.js"></script>
<script>
    layui.config({
        base: '/resources/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index']);

    function showType(ty,b){
        $(".showType").removeClass("layui-btn-disabled");
        $(b).addClass("layui-btn-disabled");
        if(ty==0){
            $(".childType").show();
        }else if(ty==1){
            $(".childType").hide();
            $(".child1").show();
        }else if(ty==2){
            $(".childType").hide();
            $(".child2").show();
        }else if(ty==3){
            $(".childType").hide();
            $(".child3").show();
        }
    }
    function getData(id){
        $(".cccc").removeClass("layui-btn-disabled");
        $(id).addClass("layui-btn-disabled");
        basePost({
            url:"/studyData.htmx",
            data:{
                childId:id,
            },
            success:function(req){
                $("#poi").html("");
                if(req){
                    for(var i=0;i<req.length;i++){
                        var fl = req[i];
                        var hop = '<div class="layui-col-xs3" onclick="opinOf(this)" oid="'+fl.id+'" > '+
                                ' <div class="layui-card" > '+
                                '<div class="layui-card-body layui-col-xs12"> '+
                                '<img src="'+fl.cover+'" style="height:120px; max-width：100px;"> '+
                                '<p>'+fl.title+'</p> '+
                                ' </div> '+
                                '</div>'+
                                '</div>';
                        $("#poi").append(hop);
                    }
                }
            }
        })
    }



    function opinOf(b){
        var id = $(b).attr("oid");
        var href="/article/info.htmx?id="+id;
        window.open(href);
    }
</script>

<style id="LAY_layadmin_theme">.layui-side-menu,.layadmin-pagetabs .layui-tab-title li:after,.layadmin-pagetabs .layui-tab-title li.layui-this:after,.layui-layer-admin .layui-layer-title,.layadmin-side-shrink .layui-side-menu .layui-nav>.layui-nav-item>.layui-nav-child{background-color:#20222A !important;}.layui-nav-tree .layui-this,.layui-nav-tree .layui-this>a,.layui-nav-tree .layui-nav-child dd.layui-this,.layui-nav-tree .layui-nav-child dd.layui-this a{background-color:#009688 !important;}.layui-layout-admin .layui-logo{background-color:#20222A !important;}</style></body>
</html>