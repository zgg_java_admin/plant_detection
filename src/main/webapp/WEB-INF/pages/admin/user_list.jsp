<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/resources/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/admin.css" media="all">
</head>
<body>


<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">
                    <div class="demoTable">
                        <form id="userForm" class="layui-form-pane">
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">用户名</label>
                                    <div class="layui-input-block">
                                        <input class="layui-input" name="name" id="name" value="${name}" autocomplete="off" placeholder="请输入用户名">
                                    </div>
                                </div>
                                <div class="layui-inline"style="float: right; padding-right: 7%">
                                    <a class="layui-btn" onclick="query()"><i class="layui-icon">&#xe615;</i>搜索</a>
                                    <a eh_auth="add_user" style="margin-left: 80px;" onclick="edit(this)" class="layui-btn" id="addUserBtn"> <i class="layui-icon">&#xe654;</i>添加用户</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    <table class="layui-table">
                        <colgroup>
                            <col width="300">
                            <col>
                            <col>
                        </colgroup>
                        <thead>
                            <tr>

                                <th>名称</th>
                                <th>登录名</th>
                                <th>创建时间</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${entityLists}" var="user">
                                <tr>

                                    <td>${user.name}</td>
                                    <td>${user.loginName}</td>
                                    <td>${user.createTime}</td>
                                    <td>
                                       <span class="layui-btn-xs layui-btn" bid="${user.id}" onclick="edit(this)">编辑</span>
<%--                                       <span class="layui-btn-xs layui-btn layui-btn-danger" bid="${user.id}" onclick="deleteIn(this)">删除</span>--%>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="menuContent" class="menuContent" style="display:none; position: absolute;height: 160px;overflow: auto;background-color: #e0eee6">
    <ul id="treeDemo" class="ztree" style="margin-top:0; width:172px;"></ul>
</div>

<script src="/resources/layuiadmin/layui/layui.js"></script>
<script src="/resources/jquery-2.2.4.js"></script>
<script src="/resources/base.js"></script>
<script>

    layui.config({
        base: '/resources/layuiadmin/' //静态资源(layui.js)所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'table','layer'], function () {

    });

    function query() {
        window.location.href="/adminUser/indexPage.htm?name="+$("#name").val()
    }
    
    function edit(b) {
        var id = $(b).attr("bid");
        layui.layer.open({
            type: 2,
            title: id == undefined ? '用户信息添加' : '用户信息编辑',
            area: ['600px', "400px"],
            skin: 'layui-layer-molv',
            content: "/adminUser/savePage.htm?userId="+id,
        });
    }

    function deleteIn(b) {
        var id = $(b).attr("bid");
        layui.layer.confirm('确定删除用户信息？', {
            btn: ['确定','取消'], //按钮
            skin: 'layui-layer-molv'
        }, function(){
            basePost({
                url:"/adminUser/delete.htm",
                data:{
                    id:id
                },
                success:function (req) {
                    layer.confirm('删除成功', {
                        title: '提示',
                        btn: ['确定'], //按钮
                        skin: 'layui-layer-molv',
                        closeBtn: 0
                    }, function(){
                        window.parent.layer.closeAll();
                        // 修改了自己的密码，应该重新登录
                        window.location.href="/adminUser/indexPage.htm";
                    });
                }
            })
        }, function(){
        });
    }
</script>
</body>
</html>
