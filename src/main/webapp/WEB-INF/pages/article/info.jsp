<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>${article.title}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/resources/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/admin.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/template.css" media="all">
</head>
<body>
<div class="layui-fluid layadmin-homepage-fluid">
    <div class="layui-row layui-col-space8">
        <div class="layui-col-md12">
            <div class="layui-fluid layadmin-homepage-content">
                <div class="layui-row layui-col-space20 layadmin-homepage-list-imgtxt">
                    <div class="layui-col-md12">
                        <div class="grid-demo">
                            <div class="panel-body layadmin-homepage-shadow">
                                <a href="javascript:;" class="media-left">
                                    <img src="${article.cover}" height="46px" width="46px">
                                </a>
                                <div class="media-body">
                                    <div class="pad-btm">
                                        <p class="fontColor">${article.title}</p>
                                        <p class="min-font">
                                          <span class="layui-breadcrumb" lay-separator="-">
                                            <a href="javascript:;">${article.time}</a>
                                          </span>
                                        </p>
                                    </div>
                                    <p><b>${article.summary}</b></p>
                                    <div>${article.content}</div>
                                    <div class="media-list">
                                        <c:if test="${loginFlag== true}">
                                            <a href="javascript:;" class="layui-btn" onclick="commentxx('${article.id}')">评论</a>
                                            <hr>
                                        </c:if>
                                        <c:forEach items="${comments}" var="comment">
                                            <div class="media-item">
                                                <div class="media-text">
                                                    <div>
                                                        <a href="javascript:;">${comment.userName}</a>
                                                        <mdall>${comment.time}</mdall>
                                                        <c:if test="${loginFlag== true}">
                                                            <c:if test="${article.userId == user.id}">
                                                                <c:if test="${comment.rContent==null}">
                                                                    <span style="margin-left: 50px;" class="layui-btn layui-btn-xs" onclick="huifu('${comment.id}')">回复</span>
                                                                </c:if>
                                                            </c:if>
                                                        </c:if>
                                                    </div>
                                                    <div><b>评论:${comment.content}</b></div>
                                                    <c:if test="${comment.rContent!=null}">
                                                        <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;回复:${comment.rContent}</div>
                                                    </c:if>

                                                </div>
                                            </div>
                                            <hr>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/resources/layuiadmin/layui/layui.js"></script>
<script src="/resources/jquery-2.2.4.js"></script>
<script src="/resources/base.js"></script>

<script>

    layui.config({

        base: '/resources/layuiadmin/' //静态资源所在路径

    }).extend({

        index: 'lib/index' //主入口模块

    }).use(['index']);


    function commentxx(id) {
        layui.layer.prompt({title: '用户评论', formType: 2}, function(text, index){
            basePost({
                url:"/article/commentSave.htmx",
                data:{
                    aid:id,
                    content:text
                },
                success:function (req) {
                    layer.confirm('感谢您的评论', {
                        title: '提示',
                        btn: ['确定'], //按钮
                        skin: 'layui-layer-molv',
                        closeBtn: 0
                    }, function(){
                        window.layer.closeAll();
                        window.location.href="/article/info.htmx?id="+id;
                    });
                }
            });
        });
    }
    
    function huifu(id) {
        layui.layer.prompt({title: '回复评论', formType: 2}, function(text, index){
            basePost({
                url:"/article/commentSaveRs.htmx",
                data:{
                    cid:id,
                    content:text
                },
                success:function (req) {
                    layer.confirm('评论已回复', {
                        title: '提示',
                        btn: ['确定'], //按钮
                        skin: 'layui-layer-molv',
                        closeBtn: 0
                    }, function(){
                        window.layer.closeAll();
                        window.location.href="/article/info.htmx?id=${article.id}";
                    });
                }
            });
        });
    }

</script>

</body>

</html>