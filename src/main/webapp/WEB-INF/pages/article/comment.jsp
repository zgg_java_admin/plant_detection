<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/resources/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/admin.css" media="all">
</head>
<body>


<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">
                    <table class="layui-table">
                        <colgroup>
                            <col width="200">
                            <col  width="200">
                            <col>
                            <col  width="300">
                            <col  width="100">
                        </colgroup>
                        <thead>
                            <tr>
                                <th>时间</th>
                                <th>评论用户</th>
                                <th>评论内容</th>
                                <th>回复内容</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${articleCommentEntities}" var="comment">
                                <tr>
                                    <td>${comment.time}</td>
                                    <td>${comment.userName}</td>
                                    <td>${comment.content}</td>
                                    <td>${comment.rContent}</td>
                                    <td>
                                       <span class="layui-btn-xs layui-btn layui-btn-danger" bid="${comment.id}" onclick="deleteIn(this)">删除</span>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="menuContent" class="menuContent" style="display:none; position: absolute;height: 160px;overflow: auto;background-color: #e0eee6">
    <ul id="treeDemo" class="ztree" style="margin-top:0; width:172px;"></ul>
</div>

<script src="/resources/layuiadmin/layui/layui.js"></script>
<script src="/resources/jquery-2.2.4.js"></script>
<script src="/resources/base.js"></script>
<script>

    layui.config({
        base: '/resources/layuiadmin/' //静态资源(layui.js)所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'table','layer'], function () {

    });

    function query() {
        window.location.href="/adminUser/indexPage.htm?name="+$("#name").val()
    }

    function deleteIn(b) {
        var id = $(b).attr("bid");
        layui.layer.confirm('确定？', {
            btn: ['确定','取消'], //按钮
            skin: 'layui-layer-molv'
        }, function(){
            basePost({
                url:"/article/del.htm",
                data:{
                    id:id
                },
                success:function (req) {
                    layer.confirm('删除成功', {
                        title: '提示',
                        btn: ['确定'], //按钮
                        skin: 'layui-layer-molv',
                        closeBtn: 0
                    }, function(){
                        window.layer.closeAll();
                        // 修改了自己的密码，应该重新登录
                        window.location.href="/article/comment.htm?id=${id}";
                    });
                }
            })
        }, function(){
        });
    }
</script>
</body>
</html>
