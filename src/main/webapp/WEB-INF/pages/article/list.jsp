<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/resources/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/resources/layuiadmin/style/admin.css" media="all">
</head>
<body>


<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">
                    <div class="demoTable">
                        <form id="userForm" class="layui-form-pane">
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">标题</label>
                                    <div class="layui-input-block">
                                        <input class="layui-input" name="title"   autocomplete="off" >
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">摘要</label>
                                    <div class="layui-input-block">
                                        <input class="layui-input" name="summary"  autocomplete="off" >
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">标签</label>
                                    <div class="layui-input-block">
                                        <input class="layui-input" name="label"  autocomplete="off" >
                                    </div>
                                </div>
                                <div class="layui-inline"style="float: right; padding-right: 7%">
                                    <a class="layui-btn" id="selectBtn"><i class="layui-icon">&#xe615;</i>搜索</a>
                                    <a  style="margin-left: 80px;" onclick="add(this)" class="layui-btn" id="addUserBtn">发表文章</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    <table class="layui-hide" id="test-table-checkbox" ></table>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="/resources/layuiadmin/layui/layui.js"></script>
<script src="/resources/jquery-2.2.4.js"></script>
<script src="/resources/base.js"></script>
<script>

    layui.config({
        base: '/resources/layuiadmin/' //静态资源(layui.js)所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'table','layer'], function () {
        var table = layui.table;
        // 定义数据
        var data = [
            {field: 'title', width: 200, title: '标题', align: "center"}
            ,{field: 'summary', width: 400, title: '摘要', align: "center"}
            ,{field: 'label', width: 200, title: '标签', align: "center"}
            ,{field: 'time', width: 170, title: '发布时间', align: "center"}
            , {
                field: 'type', title: '一级分类', width: 100,  align: "center", templet: function (d) {
                    if (d.type == 1) {
                        return '植被'
                    }
                    else if (d.type == 2) {
                        return '害虫'
                    }
                    else if (d.type == 3) {
                        return '果蔬'
                    }
                }
            }
            , {
                field: 'childName', title: '二级分类', width: 100,  align: "center"
            }
            , {
                field: 'top', title: '置顶', width: 80,  align: "center", templet: function (d) {
                    if (d.top == 1) {
                        return '<span style="color: red">置顶</span>'
                    }
                    else  {
                        return '未置顶'
                    }
                }
            }
            , {
                field: 'status', title: '状态', width: 120,  align: "center", templet: function (d) {
                    if (d.status == 1) {
                        return '<span style="color: green">审核通过</span>'
                    }
                    else if (d.status == 2) {
                        return '<span style="color: red">审核拒绝</span>'
                    }
                }
            }
            , {
                field: 'userName', title: '用户', width: 120,  align: "center", templet: function (d) {
                    if (d.userName == null || d.userName=="") {
                        return '<span style="color: red">系统发布</span>'
                    }
                    else {
                        return d.userName;
                    }
                }
            }
            , {
                fixed: 'right', title: '操作', minWidth: 140,  align: "center", templet: function (d) {
                   var html = "";
                   html+= '<span oid="'+d.id+'" onclick="edit(this)" class="layui-btn-xs layui-btn ">编辑</span>';
                   html+= '<span oid="'+d.id+'" onclick="comment(this)" class="layui-btn-xs layui-btn ">评论列表</span>';
                   return html;
                }
            }
        ];
        // 渲染表格数据
        table.render({
            elem: '#test-table-checkbox' // 表格id
            , url: '/article/page.htm' // 数据接口（用于获取数据）
            , cols: [data] // 表格数据
            , page: true // 开启分页
            , limit: 10 // 每页数据条数
            , limits: [10, 30, 50, 100] // 设置可供选择的每页数据量
            , even: true
            , id: "user_list"
            , done: function () {

            }
        });
        var $ = layui.$;
        $("#selectBtn").on('click', function () {
            tableReload();
        });
    });

    function tableReload() {
        var data = getFormData("userForm");
        layui.table.reload('user_list', {
            page: {
                curr: 1 //重新从第 1 页开始
            }
            , where: data
        });
    }


    
    function add(b) {
        layui.layer.open({
            type: 2,
            title: '发表文章',
            area: ['90%', "600px"],
            skin: 'layui-layer-molv',
            content: "/article/add.htm",
        });
    }


    function edit(b) {
        var id = $(b).attr("oid");
        layui.layer.open({
            type: 2,
            title: '编辑文章',
            area: ['90%', "600px"],
            skin: 'layui-layer-molv',
            content: "/article/edit.htm?id="+id,
        });
    }
    
    function comment(b) {
        var id = $(b).attr("oid");
        layui.layer.open({
            type: 2,
            title: '评论列表',
            area: ['90%', "600px"],
            skin: 'layui-layer-molv',
            content: "/article/comment.htm?id="+id,
        });
    }

</script>
</body>
</html>
