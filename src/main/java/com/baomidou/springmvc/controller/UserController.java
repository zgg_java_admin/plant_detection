package com.baomidou.springmvc.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.baomidou.springmvc.common.PageVo;
import com.baomidou.springmvc.common.Result;
import com.baomidou.springmvc.model.plant.UserEntity;
import com.baomidou.springmvc.service.UserService;
import com.baomidou.springmvc.table.UserTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 普通用户管理
 */
@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 列表
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("indexPage.htm")
    public String indexPage(Model model)throws Exception{
       return "user/user_list";
    }

    /**
     * 分页
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("page.htm")
    @ResponseBody
    public PageVo indexPage(Model model, String name, int page ,int limit)throws Exception{

        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.like(UserTable.NAME,name);


        Page<UserEntity> paged = new Page();
        paged.setSize(limit);
        paged.setCurrent(page);
        Page<UserEntity> userTablePage = userService.selectPage(paged, entityWrapper);

        PageVo<UserEntity> pageVo = new PageVo<>();
        pageVo.setCode(0);
        pageVo.setCount(paged.getTotal());
        pageVo.setData(userTablePage.getRecords());
        pageVo.setPageNum(limit);
        pageVo.setPageSize(page);
        return pageVo;

    }


    /**
     * 保存界面
     * @param model
     * @param userId
     * @return
     */
    @GetMapping("savePage.htm")
    public String savePage(Model model ,String userId){
        UserEntity entity = new UserEntity();
        if(userId!=null && !userId.equals("")){
            entity = userService.selectById(userId);
        }
        model.addAttribute("vo",entity);
        return  "user/user_save";
    }



    /**
     * 保存
     * @param model
     * @return
     */
    @PostMapping("save.htm")
    @ResponseBody
    public Result savePage(Model model , UserEntity entity){
        if(entity.getId()==null || entity.getId().equals("")){
            entity.setId(IdWorker.get32UUID());
            entity.setSalt(IdWorker.get32UUID());
            userService.insert(entity);
        }else{
            userService.updateById(entity);
        }
        return Result.success("用户保存成功");
    }

    /**
     * 用户删除
     * @param model
     * @param id
     * @return
     */
    @PostMapping("delete.htm")
    @ResponseBody
    public Result delete(Model model , String id){
        userService.deleteById(id);
        return Result.success("用户删除成功");
    }



}
