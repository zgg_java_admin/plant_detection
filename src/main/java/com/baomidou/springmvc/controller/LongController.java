package com.baomidou.springmvc.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.springmvc.common.Result;
import com.baomidou.springmvc.mapper.ArticleDao;
import com.baomidou.springmvc.mapper.UserReadArticleDao;
import com.baomidou.springmvc.model.plant.ArticleEntity;
import com.baomidou.springmvc.model.plant.ChildTypeEntity;
import com.baomidou.springmvc.service.*;
import com.baomidou.springmvc.table.ArticleTable;
import com.baomidou.springmvc.table.ChildTypeTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Administrator on 2019/12/28.
 */
@Controller
public class LongController {

    @Autowired
    private ChildTypeService childTypeService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private ArticleDao articleDao;


    @GetMapping("study.htmx")
    public String study(Model model){

        List<ChildTypeEntity> list = childTypeService.selectList(new EntityWrapper<ChildTypeEntity>());
        model.addAttribute("childTypes",list);

        return "study";
    }

    @PostMapping("studyData.htmx")
    @ResponseBody
    public Result getData(String childId){
        List<ArticleEntity> list = null;
        if(StringUtils.isEmpty(childId)){
            list = articleDao.listll();
        }else{
            list = articleDao.selectList(new EntityWrapper<ArticleEntity>());
            if(list!=null && !list.isEmpty()){
                for (int i = list.size()-1; i >=0 ; i--) {
                    ArticleEntity ar = list.get(i);
                    if(ar.getUserId().equals("-1") || !ar.getChildType().trim().equals(childId.trim())){
                        list.remove(i);
                    }
                }
            }
        }
        return Result.success(list);
    }


}
