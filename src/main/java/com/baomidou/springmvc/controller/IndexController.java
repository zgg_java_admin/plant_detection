package com.baomidou.springmvc.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.springmvc.common.Result;
import com.baomidou.springmvc.model.plant.AdminUserEntity;
import com.baomidou.springmvc.service.AdminUserService;
import com.baomidou.springmvc.table.AdminUserTable;
import jdk.nashorn.internal.ir.ReturnNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
public class IndexController {

    @Autowired
    private AdminUserService adminUserService;

    /**
     * 首页
     * @return
     */
    @GetMapping("/admin/index.htm")
    public String index(){
        return "index";
    }



    /**
     * 首页
     * @return
     */
    @GetMapping("/admin/login.htm")
    public String login(){
        return "login";
    }

    /**
     * 首页
     * @return
     */
    @GetMapping("/admin/logOut.htm")
    public String logOut(HttpSession session){
        session.removeAttribute("adminUser");
        return "login";
    }



    /**
     * 首页
     * @return
     */
    @PostMapping("/admin/loginData.htm")
    @ResponseBody
    public Result loginData(String userName, String passWord, HttpSession session){
        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.eq(AdminUserTable.LOGIN_NAME,userName);

        AdminUserEntity adminUserEntity = adminUserService.selectOne(entityWrapper);
        if(adminUserEntity==null){
            return Result.error("用户或者密码错误");
        }
        if(adminUserEntity.getPwd()!=null && adminUserEntity.getPwd().equals(passWord)){
            session.setAttribute("adminUser",adminUserEntity);
            return Result.success("登录成功");
        }
        return Result.error("用户或者密码错误");
    }
}
