package com.baomidou.springmvc.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.baomidou.springmvc.baidu.BaiDuAiResukt;
import com.baomidou.springmvc.common.Result;
import com.baomidou.springmvc.mapper.UserReadArticleDao;
import com.baomidou.springmvc.model.plant.ArticleEntity;
import com.baomidou.springmvc.model.plant.ImgAiDistinguishEntity;
import com.baomidou.springmvc.model.plant.UserEntity;
import com.baomidou.springmvc.model.plant.UserOpinionEntity;
import com.baomidou.springmvc.service.*;
import com.baomidou.springmvc.table.ArticleTable;
import com.baomidou.springmvc.table.ImgAiDistinguishTable;
import com.baomidou.springmvc.table.UserOpinionTable;
import com.baomidou.springmvc.util.AnimalUtil;
import com.baomidou.springmvc.util.IngredientUtil;
import com.baomidou.springmvc.util.plant.PlantUtil;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("userInfo")
public class UserInfoController {

    @Autowired
    private UserReadArticleDao userReadArticleDao;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private ArticleCommentService articleCommentService;

    @Autowired
    private UserOpinionService userOpinionService;

    @Autowired
    private ImgAiDistinguishService imgAiDistinguishService;


    @GetMapping("info.htmx")
    public String infos(Model model, HttpSession session)throws Exception{
         UserEntity userEntity = (UserEntity) session.getAttribute("user");
         if(userEntity==null){
             return "home/index";
         }
         String userId = userEntity.getId();

        //用户的足迹 浏览历史
        List<ArticleEntity> articleEntities = userReadArticleDao.selectByUserId(userId);
        model.addAttribute("zujis",articleEntities==null?new ArrayList<>():articleEntities);

        //文章列表
        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.eq(ArticleTable.STATUS,1);
        entityWrapper.eq(ArticleTable.USER_ID,userId).orderBy("time",false);
        List<ArticleEntity> articleEntityist = articleService.selectList(entityWrapper);
        model.addAttribute("articleEntityist",articleEntityist==null?new ArrayList<>():articleEntityist);


        //识别历史
        entityWrapper = new EntityWrapper();
        entityWrapper.eq(ImgAiDistinguishTable.USER_ID,userId).orderBy("time",false);
        List<ImgAiDistinguishEntity> imgAiDistinguishEntities = imgAiDistinguishService.selectList(entityWrapper);
        model.addAttribute("imgAiDistinguishEntities",imgAiDistinguishEntities==null?new ArrayList<>():imgAiDistinguishEntities);

        //意见
        entityWrapper = new EntityWrapper();
        entityWrapper.eq(UserOpinionTable.USER_ID,userId).orderBy("time",false);
        List<UserOpinionEntity> userOpinionEntities =   userOpinionService.selectList(entityWrapper);
        model.addAttribute("userOpinionEntities",userOpinionEntities==null?new ArrayList<>():userOpinionEntities);

        return "userInfo/info";
    }

    @GetMapping("aiImg.htmx")
    public String aiImg(Model model,Integer type)throws Exception{
        model.addAttribute("type",type);
        return "userInfo/aiImg";
    }

    @PostMapping("aiImgData.htmx")
    @ResponseBody
    public Result aiImgData(Model model, Integer type, String fileName,HttpSession session)throws Exception{
        UserEntity userEntity = (UserEntity) session.getAttribute("user");
        String userId = userEntity.getId();
        BaiDuAiResukt baiDuAiResukt = null;
        if(type==1){
             baiDuAiResukt =  PlantUtil.plant(ImgController.FILE_PATH+"\\"+fileName);
        }else if(type==2){
             baiDuAiResukt =  AnimalUtil.animal(ImgController.FILE_PATH+"\\"+fileName);
        }else if(type==3){
             baiDuAiResukt =  IngredientUtil.ingredient(ImgController.FILE_PATH+"\\"+fileName);
        }
        ImgAiDistinguishEntity aiDistinguishEntity = new ImgAiDistinguishEntity();
        aiDistinguishEntity.setImgUrl("/img/pic?pictureName="+fileName);
        aiDistinguishEntity.setResult(JSON.toJSONString(baiDuAiResukt));
        aiDistinguishEntity.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        aiDistinguishEntity.setType(type);
        aiDistinguishEntity.setUserId(userId);
        aiDistinguishEntity.setId(IdWorker.get32UUID());
        imgAiDistinguishService.insert(aiDistinguishEntity);



        return Result.success(baiDuAiResukt,"识别成功");
    }


    @GetMapping("imgInfo.htmx")
    public String imgInfo(Model model,String id)throws Exception{
        ImgAiDistinguishEntity aiDistinguishEntity =imgAiDistinguishService.selectById(id);
        model.addAttribute("aiDistinguishEntity",aiDistinguishEntity);
        return "userInfo/aiImgInfo";
    }

    @PostMapping("userOpinion.htmx")
    @ResponseBody
    public Result userOpinion(Model model,String text,HttpSession session)throws Exception{
        UserEntity userEntity = (UserEntity) session.getAttribute("user");
        String userId = userEntity.getId();
        UserOpinionEntity entity = new UserOpinionEntity();
        entity.setContent(text);
        entity.setId(IdWorker.get32UUID());
        entity.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        entity.setUserId(userId);
        userOpinionService.insert(entity);
        return Result.success("感谢您的意见与建议");
    }


}
