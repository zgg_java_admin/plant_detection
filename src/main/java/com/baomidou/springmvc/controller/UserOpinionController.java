package com.baomidou.springmvc.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.baomidou.springmvc.common.PageVo;
import com.baomidou.springmvc.common.Result;
import com.baomidou.springmvc.model.plant.UserEntity;
import com.baomidou.springmvc.model.plant.UserOpinionEntity;
import com.baomidou.springmvc.service.UserOpinionService;
import com.baomidou.springmvc.service.UserService;
import com.baomidou.springmvc.table.UserOpinionTable;
import com.baomidou.springmvc.table.UserTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * 普通用户意见管理管理
 */
@Controller
@RequestMapping("userOpinion")
public class UserOpinionController {

    @Autowired
    private UserOpinionService userOpinionService;

    @Autowired
    private UserService userService;

    /**
     * 列表
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("indexPage.htm")
    public String indexPage(Model model)throws Exception{
       return "userOpinion/list";
    }

    /**
     * 分页
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("page.htm")
    @ResponseBody
    public PageVo indexPage(Model model, String content, int page ,int limit)throws Exception{

        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.like(UserOpinionTable.CONTENT,content);
        entityWrapper.orderBy("repaly_flag");

        Page<UserOpinionEntity> paged = new Page();
        paged.setSize(limit);
        paged.setCurrent(page);
        Page<UserOpinionEntity> userTablePage = userOpinionService.selectPage(paged, entityWrapper);

        List<UserOpinionEntity> list =  userTablePage.getRecords();
        if(list!=null){
            for (UserOpinionEntity userOpinionEntity : list) {
                UserEntity userEntity = userService.selectById(userOpinionEntity.getUserId());
                if(userEntity!=null){
                    userOpinionEntity.setUserName(userEntity.getName());
                }
            }
        }

        PageVo<UserOpinionEntity> pageVo = new PageVo<>();
        pageVo.setCode(0);
        pageVo.setCount(paged.getTotal());
        pageVo.setData(userTablePage.getRecords());
        pageVo.setPageNum(limit);
        pageVo.setPageSize(page);
        return pageVo;

    }

    /**
     * 保存
     * @param model
     * @return
     */
    @PostMapping("save.htm")
    @ResponseBody
    public Result savePage(Model model , UserOpinionEntity entity){
        if(entity.getId()==null || entity.getId().equals("")){
            entity.setId(IdWorker.get32UUID());
            userOpinionService.insert(entity);
        }else{
            userOpinionService.updateById(entity);
        }
        return Result.success("保存成功");
    }



}
