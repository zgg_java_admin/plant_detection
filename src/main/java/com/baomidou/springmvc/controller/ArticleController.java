package com.baomidou.springmvc.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.baomidou.springmvc.common.PageVo;
import com.baomidou.springmvc.common.Result;
import com.baomidou.springmvc.model.plant.*;
import com.baomidou.springmvc.service.*;
import com.baomidou.springmvc.table.ArticleCommentTable;
import com.baomidou.springmvc.table.ArticleTable;
import com.baomidou.springmvc.table.UserReadArticleTable;
import com.baomidou.springmvc.util.AipImageCensorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 文章
 */
@Controller
@RequestMapping("article")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private UserService userService;

    @Autowired
    private ChildTypeService childTypeService;

    @Autowired
    private ArticleCommentService articleCommentService;

    @Autowired
    private UserReadArticleService userReadArticleService;

    /**
     * 界面
     * @return
     * @throws Exception
     */
    @GetMapping("list.htm")
    public String list()throws Exception{
        return "article/list";
    }

    /**
     * 界面
     * @return
     * @throws Exception
     */
    @GetMapping("list2.htm")
    public String list2()throws Exception{
        return "article/list2";
    }


    /**
     * 分页
     * @return
     * @throws Exception
     */
    @GetMapping("page2.htm")
    @ResponseBody
    public PageVo page2(String title, String  summary, String label,  int page , int limit)throws Exception{
        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.eq(ArticleTable.USER_ID,"-1");
        if(!StringUtils.isEmpty(title)){
            entityWrapper.like(ArticleTable.TITLE,title);
        }
        if(!StringUtils.isEmpty(summary)){
            entityWrapper.like(ArticleTable.SUMMARY,summary);
        }
        if(!StringUtils.isEmpty(label)){
            entityWrapper.like(ArticleTable.LABEL,label);
        }
        entityWrapper.orderBy("top",false).orderBy("time",false);

        Page<ArticleEntity> paged = new Page();
        paged.setSize(limit);
        paged.setCurrent(page);
        Page<ArticleEntity> userTablePage = articleService.selectPage(paged, entityWrapper);

        List<ArticleEntity> list = userTablePage.getRecords();
        if(list!=null && !list.isEmpty()){
            for (ArticleEntity articleEntity : list) {
                UserEntity userEntity = userService.selectById(articleEntity.getUserId());
                if(userEntity!=null){
                    articleEntity.setUserName(userEntity.getName());
                }
                ChildTypeEntity childTypeEntity = childTypeService.selectById(articleEntity.getChildType());
                if(childTypeEntity!=null){
                    articleEntity.setChildName(childTypeEntity.getName());
                }
            }
        }
        PageVo<ArticleEntity> pageVo = new PageVo<>();
        pageVo.setCode(0);
        pageVo.setCount(paged.getTotal());
        pageVo.setData(list);
        pageVo.setPageNum(limit);
        pageVo.setPageSize(page);
        return pageVo;

    }


    /**
     * 分页
     * @return
     * @throws Exception
     */
    @GetMapping("page.htm")
    @ResponseBody
    public PageVo page(String title, String  summary, String label,  int page , int limit)throws Exception{
        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.ne(ArticleTable.USER_ID, "-1");
        if(!StringUtils.isEmpty(title)){
            entityWrapper.like(ArticleTable.TITLE,title);
        }
        if(!StringUtils.isEmpty(summary)){
            entityWrapper.like(ArticleTable.SUMMARY,summary);
        }
        if(!StringUtils.isEmpty(label)){
            entityWrapper.like(ArticleTable.LABEL,label);
        }
        entityWrapper.orderBy("top",false).orderBy("time",false);

        Page<ArticleEntity> paged = new Page();
        paged.setSize(limit);
        paged.setCurrent(page);
        Page<ArticleEntity> userTablePage = articleService.selectPage(paged, entityWrapper);

        List<ArticleEntity> list = userTablePage.getRecords();
        if(list!=null && !list.isEmpty()){
            for (ArticleEntity articleEntity : list) {
                UserEntity userEntity = userService.selectById(articleEntity.getUserId());
                if(userEntity!=null){
                    articleEntity.setUserName(userEntity.getName());
                }
                ChildTypeEntity childTypeEntity = childTypeService.selectById(articleEntity.getChildType());
                if(childTypeEntity!=null){
                    articleEntity.setChildName(childTypeEntity.getName());
                }
            }
        }
        PageVo<ArticleEntity> pageVo = new PageVo<>();
        pageVo.setCode(0);
        pageVo.setCount(paged.getTotal());
        pageVo.setData(list);
        pageVo.setPageNum(limit);
        pageVo.setPageSize(page);
        return pageVo;

    }

    @GetMapping("page.htmx")
    @ResponseBody
    public PageVo pagex(Integer type, String  label, String title,  int page , int limit)throws Exception{
        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.ne(ArticleTable.USER_ID,"-1");
        if(!StringUtils.isEmpty(type)){
            entityWrapper.eq(ArticleTable.TYPE,type);
        }
        if(!StringUtils.isEmpty(label)){
            entityWrapper.like(ArticleTable.LABEL,label);
        }
        if(!StringUtils.isEmpty(title)){
            entityWrapper.like(ArticleTable.TITLE,title);
        }
        entityWrapper.orderBy("time",false);

        Page<ArticleEntity> paged = new Page();
        paged.setSize(limit);
        paged.setCurrent(page);
        Page<ArticleEntity> userTablePage = articleService.selectPage(paged, entityWrapper);

        List<ArticleEntity> list = userTablePage.getRecords();
        if(list!=null && !list.isEmpty()){
            for (ArticleEntity articleEntity : list) {
                UserEntity userEntity = userService.selectById(articleEntity.getUserId());
                if(userEntity!=null){
                    articleEntity.setUserName(userEntity.getName());
                }
                ChildTypeEntity childTypeEntity = childTypeService.selectById(articleEntity.getChildType());
                if(childTypeEntity!=null){
                    articleEntity.setChildName(childTypeEntity.getName());
                }
            }
        }
        PageVo<ArticleEntity> pageVo = new PageVo<>();
        pageVo.setCode(0);
        pageVo.setCount(paged.getTotal());
        pageVo.setData(list);
        pageVo.setPageNum(limit);
        pageVo.setPageSize(page);
        return pageVo;
    }

    /**
     * 界面
     * @return
     * @throws Exception
     */
    @GetMapping("add.htm")
    public String add()throws Exception{
        return "article/add";
    }

    /**
     * 界面
     * @return
     * @throws Exception
     */
    @GetMapping("add.htmx")
    public String addx()throws Exception{
        return "home/acadd";
    }

    /**
     * 保存数据
     * @return
     * @throws Exception
     */
    @RequestMapping("addDatag.htmx")
    @ResponseBody
    public Result addDatax(ArticleEntity articleEntity,HttpSession session)throws Exception{
        UserEntity userEntity = (UserEntity) session.getAttribute("user");
        userEntity = userService.selectById(userEntity.getId());
        if(userEntity.getType()!=1){
            return Result.error("用户状态不可用哦");
        }
        articleEntity.setId(IdWorker.get32UUID());
        articleEntity.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        articleEntity.setStatus(1);
        boolean result1 = AipImageCensorUtil.discernSensitiveWords(articleEntity.getContent(), null);
        if(!result1){
            return Result.error("非法文案");
        }
        articleEntity.setUserId(userEntity.getId());
        articleService.insert(articleEntity);
        return Result.success("保存成功");
    }


    /**
     * 界面
     * @return
     * @throws Exception
     */
    @GetMapping("info.htmx")
    public String info(String id, Model model, HttpSession session)throws Exception{
        ArticleEntity articleEntity = articleService.selectById(id);
        model.addAttribute("article",articleEntity);
        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.eq(ArticleCommentTable.ARTICLE_ID,id).orderBy("time",false);
        List<ArticleCommentEntity> articleCommentEntities =  articleCommentService.selectList(entityWrapper);
        if(articleCommentEntities!=null && !articleCommentEntities.isEmpty()){
            for (ArticleCommentEntity d : articleCommentEntities) {
                UserEntity userEntity = userService.selectById(d.getUserId());
                if(userEntity!=null){
                    d.setUserName(userEntity.getName());
                }
            }
        }
        model.addAttribute("comments",articleCommentEntities);
        UserEntity userEntity =  (UserEntity) session.getAttribute("user");
        if(userEntity!=null){
            entityWrapper = new EntityWrapper();
            entityWrapper.eq(UserReadArticleTable.ARTICLE_ID,id).eq(UserReadArticleTable.USER_ID,userEntity.getId());
            UserReadArticleEntity userReadArticle = userReadArticleService.selectOne(entityWrapper);
            if(userReadArticle==null){
                userReadArticle = new UserReadArticleEntity();
                userReadArticle.setId(IdWorker.get32UUID());
                userReadArticle.setTime(new Date());
                userReadArticle.setUserId(userEntity.getId());
                userReadArticle.setArticleId(id);
                userReadArticleService.insert(userReadArticle);
            }else{
                userReadArticle.setTime(new Date());
                userReadArticleService.updateById(userReadArticle);
            }
        }
        boolean loginFlag = false;
        UserEntity pl = (UserEntity) session.getAttribute("user");
        if(pl==null){
        }else{
            loginFlag = true;
            model.addAttribute("userEntity",pl);
        }
        model.addAttribute("loginFlag",loginFlag);

        return "article/info";
    }

    /**
     * 保存数据
     * @return
     * @throws Exception
     */
    @RequestMapping("addDatag.htm")
    @ResponseBody
    public Result addData(ArticleEntity articleEntity )throws Exception{
        articleEntity.setId(IdWorker.get32UUID());
        articleEntity.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        articleEntity.setStatus(1);
        articleEntity.setUserId("-1");
        articleService.insert(articleEntity);
        return Result.success("保存成功");
    }


    /**
     * 界面
     * @return
     * @throws Exception
     */
    @GetMapping("edit.htm")
    public String edit(String id, Model model )throws Exception{
        ArticleEntity articleEntity = articleService.selectById(id);
        model.addAttribute("articleEntity",articleEntity);
        return "article/edit";
    }

    /**
     * 保存数据
     * @return
     * @throws Exception
     */
    @RequestMapping("editData.htm")
    @ResponseBody
    public Result editData(ArticleEntity articleEntity)throws Exception{
        boolean result1 = AipImageCensorUtil.discernSensitiveWords(articleEntity.getContent(), null);
        if(!result1){
            return Result.error("非法文案");
        }
        articleService.updateById(articleEntity);
        return Result.success("保存成功");
    }


    /**
     * 评论
     * @param id
     * @return
     * @throws Exception
     */
    @GetMapping("comment.htm")
    public String comment(String id,Model model)throws Exception{
        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.eq(ArticleCommentTable.ARTICLE_ID,id).orderBy("time",false);
        List<ArticleCommentEntity> articleCommentEntities =  articleCommentService.selectList(entityWrapper);
        if(articleCommentEntities!=null && !articleCommentEntities.isEmpty()){
            for (ArticleCommentEntity articleEntity : articleCommentEntities) {
                UserEntity userEntity = userService.selectById(articleEntity.getUserId());
                if(userEntity!=null){
                    articleEntity.setUserName(userEntity.getName());
                }
            }
        }

        model.addAttribute("id",id);
        model.addAttribute("articleCommentEntities",articleCommentEntities);
        return "article/comment";
    }

    /**
     * 删除评论
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("delA.htmx")
    @ResponseBody
    public Result delA(String id)throws Exception{
        articleService.deleteById(id);
        return Result.success("删除成功");
    }


    /**
     * 删除评论
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping("del.htm")
    @ResponseBody
    public Result del(String id)throws Exception{
        articleCommentService.deleteById(id);
        return Result.success("删除成功");
    }

    /**
     * @return
     * @throws Exception
     */
    @RequestMapping("commentSave.htmx")
    @ResponseBody
    public Result commentSave(String content,String aid,HttpSession session)throws Exception{
        ArticleCommentEntity articleCommentEntity = new ArticleCommentEntity();
        articleCommentEntity.setArticleId(aid);
        articleCommentEntity.setContent(content);
        articleCommentEntity.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        articleCommentEntity.setId(IdWorker.get32UUID());
        UserEntity pl = (UserEntity) session.getAttribute("user");
        articleCommentEntity.setUserId(pl.getId());
        articleCommentService.insert(articleCommentEntity);
        return Result.success("感谢您的评论");
    }

    /**
     * @return
     * @throws Exception
     */
    @RequestMapping("commentSaveRs.htmx")
    @ResponseBody
    public Result commentSaveRs(String content,String cid,HttpSession session)throws Exception{
        UserEntity userEntity = (UserEntity) session.getAttribute("user");
        ArticleCommentEntity articleCommentEntity = articleCommentService.selectById(cid);
        if(articleCommentEntity.getUserId().equals(userEntity.getId())){
            return Result.error("自己不能回复自己的评论哦！");
        }
        articleCommentEntity.setrContent(content);
        String userId = userEntity.getId();
        articleCommentEntity.setReplyId(userId);
        articleCommentService.updateById(articleCommentEntity);
        return Result.success("1");
    }



}
