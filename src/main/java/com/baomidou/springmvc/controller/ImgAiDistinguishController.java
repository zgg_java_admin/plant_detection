package com.baomidou.springmvc.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.springmvc.common.PageVo;
import com.baomidou.springmvc.common.Result;
import com.baomidou.springmvc.model.plant.ChildTypeEntity;
import com.baomidou.springmvc.model.plant.ImgAiDistinguishEntity;
import com.baomidou.springmvc.model.plant.UserEntity;
import com.baomidou.springmvc.service.ImgAiDistinguishService;
import com.baomidou.springmvc.service.UserService;
import com.baomidou.springmvc.table.ChildTypeTable;
import com.baomidou.springmvc.table.ImgAiDistinguishTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by Administrator on 2019/12/23.
 */
@Controller
@RequestMapping("imgAiDistinguish")
public class ImgAiDistinguishController {

    @Autowired
    private ImgAiDistinguishService imgAiDistinguishService;
    @Autowired
    private UserService userService;

    /**
     * 界面
     * @return
     * @throws Exception
     */
    @GetMapping("list.htm")
    public String list(Integer type,Model model)throws Exception{
        model.addAttribute("type",type);
        return "imgAi/list";
    }

    /**
     * 界面
     * @return
     * @throws Exception
     */
    @PostMapping("delete.htmx")
    @ResponseBody
    public Result delete(String  id,Model model)throws Exception{
        imgAiDistinguishService.deleteById(id);
        return Result.success("shanc");
    }



    /**
     * 分页
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("page.htm")
    @ResponseBody
    public PageVo indexPage(Model model, int page ,int limit,Integer type)throws Exception{

        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.eq(ImgAiDistinguishTable.TYPE,type);
        entityWrapper.orderBy("time",false);

        Page<ImgAiDistinguishEntity> paged = new Page();
        paged.setSize(limit);
        paged.setCurrent(page);
        Page<ImgAiDistinguishEntity> userTablePage = imgAiDistinguishService.selectPage(paged, entityWrapper);

        List<ImgAiDistinguishEntity> list = userTablePage.getRecords();
        if(list!=null && !list.isEmpty()){
            for (ImgAiDistinguishEntity imgAiDistinguishEntity : list) {
                UserEntity userEntity = userService.selectById(imgAiDistinguishEntity.getUserId());
                if(userEntity!=null){
                    imgAiDistinguishEntity.setUserName(userEntity.getName());
                }
            }
        }

        PageVo<ImgAiDistinguishEntity> pageVo = new PageVo<>();
        pageVo.setCode(0);
        pageVo.setCount(paged.getTotal());
        pageVo.setData(list);
        pageVo.setPageNum(limit);
        pageVo.setPageSize(page);
        return pageVo;
    }

}
