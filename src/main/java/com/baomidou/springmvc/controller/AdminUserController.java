package com.baomidou.springmvc.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.baomidou.springmvc.common.Result;
import com.baomidou.springmvc.model.plant.AdminUserEntity;
import com.baomidou.springmvc.service.AdminUserService;
import com.baomidou.springmvc.table.AdminUserTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 后台用户管理
 */
@Controller
@RequestMapping("adminUser")
public class AdminUserController {

    @Autowired
    private AdminUserService adminUserService;


    /**
     * 列表
     * @param model
     * @return
     * @throws Exception
     */
    @GetMapping("indexPage.htm")
    public String indexPage(Model model,String name)throws Exception{
       Wrapper wrapper = new EntityWrapper();
        wrapper.like(AdminUserTable.NAME,name);
       List<AdminUserEntity> entityLists =  adminUserService.selectList(wrapper);
       model.addAttribute("entityLists",entityLists);
       model.addAttribute("name",name);
       return "admin/user_list";
    }

    /**
     * 保存界面
     * @param model
     * @param userId
     * @return
     */
    @GetMapping("savePage.htm")
    public String savePage(Model model ,String userId){
        AdminUserEntity adminUserEntity = new AdminUserEntity();
        if(userId!=null && !userId.equals("")){
            adminUserEntity = adminUserService.selectById(userId);
        }
        model.addAttribute("adminUser",adminUserEntity);
        return  "admin/user_save";
    }



    /**
     * 保存
     * @param model
     * @return
     */
    @PostMapping("save.htm")
    @ResponseBody
    public Result savePage(Model model , AdminUserEntity adminUserEntity){
        if(adminUserEntity.getId()==null || adminUserEntity.getId().equals("")){
            adminUserEntity.setId(IdWorker.get32UUID());
            adminUserEntity.setCreateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            adminUserService.insert(adminUserEntity);
        }else{
            adminUserService.updateById(adminUserEntity);
        }
        return Result.success("用户保存成功");
    }

    /**
     * 用户删除
     * @param model
     * @param id
     * @return
     */
    @PostMapping("delete.htm")
    @ResponseBody
    public Result delete(Model model , String id){
        adminUserService.deleteById(id);
        return Result.success("用户删除成功");
    }



}
