package com.baomidou.springmvc.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.baomidou.springmvc.common.Result;
import com.baomidou.springmvc.model.plant.ArticleEntity;
import com.baomidou.springmvc.model.plant.UserEntity;
import com.baomidou.springmvc.service.ArticleService;
import com.baomidou.springmvc.service.UserService;
import com.baomidou.springmvc.table.ArticleTable;
import com.baomidou.springmvc.table.UserTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
public class HomeController {

    @Autowired
    private ArticleService articleService;
    @Autowired
    private UserService userService;


    @GetMapping("login.htmx")
    public String login(Model model, HttpSession session)throws Exception{
        return "home/login";
    }


    @PostMapping("loginData.htmx")
    @ResponseBody
    public Result loginData(String userName, String passWord, HttpSession session)throws Exception{
        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.eq(UserTable.EAMIL,userName);

        UserEntity userEntity = userService.selectOne(entityWrapper);
        if(userEntity==null){
            return Result.error("用户或者密码错误");
        }
        if(userEntity.getType()==0){
            return Result.error("用户还未审核，请耐心等待");
        }
        if(userEntity.getType()==2){
            return Result.error("用户已被封禁");
        }
        if(userEntity.getPwd()!=null && userEntity.getPwd().equals(passWord)){
            session.setAttribute("user",userEntity);
            userEntity.setLastLoginTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            userService.updateById(userEntity);
            return Result.success("登录成功");
        }
        return Result.error("用户或者密码错误");
    }



    @GetMapping("reg.htmx")
    public String reg(Model model)throws Exception{
        return "home/reg";
    }


    public static boolean isEmail(String email){
        if (null==email || "".equals(email)){
            return false;
        }
        String regEx1 = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
        Pattern p = Pattern.compile(regEx1);
        Matcher m = p.matcher(email);
        if(m.matches()){
            return true;
        }else{
            return false;
        }
    }
    public static void main(String[] args) {
        String msg = "1dasd23424@qq。cmn";
        System.out.println(isEmail(msg));
    }


    /**
     * 用户注册
     * @param name
     * @param userName
     * @param passWord
     * @param session
     * @return
     * @throws Exception
     */
    @PostMapping("regData.htmx")
    @ResponseBody
    public Result regData(String name,String userName, String passWord, HttpSession session)throws Exception{
        if(!isEmail(userName)){
            return Result.error("邮箱格式不正确");
        }
        if(StringUtils.isEmpty(passWord) || passWord.length()>6){
            return Result.error("密码格式错误");
        }
        EntityWrapper entityWrapper = new EntityWrapper();
        entityWrapper.eq(UserTable.EAMIL,userName);

        UserEntity userEntity = userService.selectOne(entityWrapper);
        if(userEntity!=null){
            return Result.error("用户已存在，请更换邮箱");
        }
        userEntity = new UserEntity();
        userEntity.setSalt(IdWorker.get32UUID());
        userEntity.setId(IdWorker.get32UUID());
        userEntity.setName(name);
        userEntity.setPwd(passWord);
        userEntity.setEamil(userName);
        userEntity.setType(0);
        userService.insert(userEntity);

        return Result.success("用户注册成功，请等待管理员审核");
    }



    @GetMapping("/logOut.htmx")
    public String logOut(Model model,HttpSession session)throws Exception{
        session.removeAttribute("user");
        return index(model,session);
    }


    @GetMapping("/")
    public String index(Model model,HttpSession session)throws Exception{
        boolean loginFlag = false;
        UserEntity pl = (UserEntity) session.getAttribute("user");
        if(pl==null){
        }else{
            loginFlag = true;
            model.addAttribute("userEntity",pl);
        }
        model.addAttribute("loginFlag",loginFlag);


        //查询置顶
        EntityWrapper wrapper = new EntityWrapper();
        wrapper.eq(ArticleTable.STATUS,1).eq("top",1).orderBy("time",false);
        List<ArticleEntity> topArticle =  articleService.selectList(wrapper);
        if(topArticle!=null && !topArticle.isEmpty()){
            for (ArticleEntity articleEntity : topArticle) {
                articleEntity.setUserName("系统发布");
                UserEntity userEntity = userService.selectById(articleEntity.getUserId());
                if(userEntity!=null){
                    articleEntity.setUserName(userEntity.getName());
                }
            }
        }
        model.addAttribute("topArticle",topArticle);

        Map<String,Integer> mnap = new HashMap<>();
        mnap.put("all",0);
        mnap.put("zhizhu",0);
        mnap.put("haichong",0);
        mnap.put("guoshu",0);


        Map<String,Set<String>> map = new HashMap<>();
        map.put("all",new HashSet());
        map.put("zhizhu",new HashSet());
        map.put("haichong",new HashSet());
        map.put("guoshu",new HashSet());

        //查询种类数量以及标签
        wrapper = new EntityWrapper();
        wrapper.eq(ArticleTable.STATUS,1).orderBy("time",false);
        List<ArticleEntity> allList =  articleService.selectList(wrapper);
        if(allList!=null && !allList.isEmpty()){
            mnap.put("all",allList.size());
            for (ArticleEntity articleEntity : allList) {
                if(articleEntity.getType()==1){
                    mnap.put("zhizhu",mnap.get("zhizhu")+1);
                }else if(articleEntity.getType()==2){
                    mnap.put("haichong",mnap.get("haichong")+1);
                }else if(articleEntity.getType()==3){
                    mnap.put("guoshu",mnap.get("guoshu")+1);
                }
                String leabe = articleEntity.getLabel();
                if(leabe!=null && !"".equals(leabe)){
                    String ss[] = leabe.split(",");
                    Set set = null;
                    if(articleEntity.getType()==1){
                        set = map.get("zhizhu");
                    }else if(articleEntity.getType()==2){
                        set = map.get("haichong");
                    }else if(articleEntity.getType()==3){
                        set = map.get("guoshu");
                    }
                    for (String s : ss) {
                        set.add(s);
                        map.get("all").add(s);
                    }
                }
            }
        }

        model.addAttribute("mnap",mnap);
        model.addAttribute("map",map);

        return "home/index";
    }
}
