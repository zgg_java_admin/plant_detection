package com.baomidou.springmvc.service;


import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springmvc.model.plant.UserEntity;

/**
 * 系统用户表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
public interface UserService extends IService<UserEntity> {
}