package com.baomidou.springmvc.service;


import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springmvc.model.plant.UserReadArticleEntity;

/**
 * 用户浏览文章表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
public interface UserReadArticleService extends IService<UserReadArticleEntity> {
}