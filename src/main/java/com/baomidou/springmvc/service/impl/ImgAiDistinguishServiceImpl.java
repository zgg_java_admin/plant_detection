package com.baomidou.springmvc.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.springmvc.mapper.ImgAiDistinguishDao;
import com.baomidou.springmvc.model.plant.ImgAiDistinguishEntity;
import com.baomidou.springmvc.service.ImgAiDistinguishService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 百度AI识别结果管理
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ImgAiDistinguishServiceImpl extends ServiceImpl<ImgAiDistinguishDao, ImgAiDistinguishEntity> implements ImgAiDistinguishService {
}