package com.baomidou.springmvc.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.springmvc.mapper.ArticleCommentDao;
import com.baomidou.springmvc.model.plant.ArticleCommentEntity;
import com.baomidou.springmvc.service.ArticleCommentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 文章评论表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArticleCommentServiceImpl extends ServiceImpl<ArticleCommentDao, ArticleCommentEntity> implements ArticleCommentService {
}