package com.baomidou.springmvc.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.springmvc.mapper.ChildTypeDao;
import com.baomidou.springmvc.model.plant.ChildTypeEntity;
import com.baomidou.springmvc.service.ChildTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 二级分类表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChildTypeServiceImpl extends ServiceImpl<ChildTypeDao, ChildTypeEntity> implements ChildTypeService {
}