package com.baomidou.springmvc.service.impl;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.springmvc.mapper.UserOpinionDao;
import com.baomidou.springmvc.model.plant.UserOpinionEntity;
import com.baomidou.springmvc.service.UserOpinionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户意见表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserOpinionServiceImpl extends ServiceImpl<UserOpinionDao, UserOpinionEntity> implements UserOpinionService {
}