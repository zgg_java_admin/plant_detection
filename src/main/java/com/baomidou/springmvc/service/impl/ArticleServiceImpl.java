package com.baomidou.springmvc.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.springmvc.mapper.ArticleDao;
import com.baomidou.springmvc.model.plant.ArticleEntity;
import com.baomidou.springmvc.service.ArticleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 文章表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArticleServiceImpl extends ServiceImpl<ArticleDao, ArticleEntity> implements ArticleService {
}