package com.baomidou.springmvc.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.springmvc.mapper.UserReadArticleDao;
import com.baomidou.springmvc.model.plant.UserReadArticleEntity;
import com.baomidou.springmvc.service.UserReadArticleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户浏览文章表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserReadArticleServiceImpl extends ServiceImpl<UserReadArticleDao, UserReadArticleEntity> implements UserReadArticleService {
}