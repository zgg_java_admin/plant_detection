package com.baomidou.springmvc.service;


import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springmvc.model.plant.ImgAiDistinguishEntity;

/**
 * 百度AI识别结果管理
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
public interface ImgAiDistinguishService extends IService<ImgAiDistinguishEntity> {
}