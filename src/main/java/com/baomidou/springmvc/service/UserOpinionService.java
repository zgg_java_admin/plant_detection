package com.baomidou.springmvc.service;


import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springmvc.model.plant.UserOpinionEntity;

/**
 * 用户意见表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
public interface UserOpinionService extends IService<UserOpinionEntity> {
}