package com.baomidou.springmvc.common;

import com.baomidou.springmvc.common.exception.NeedLoginException;
import com.baomidou.springmvc.model.plant.AdminUserEntity;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class CustomInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uri = request.getRequestURI();
        if(uri.startsWith("/img") || "/admin/login.htm".equals(uri)
                || "/admin/loginData.htm".equals(uri)
                || "/childType/childList.htm".equals(uri)
                || "/article/edit.htm".equals(uri)
                ){
            return true;
        }
        AdminUserEntity adminUser = (AdminUserEntity) request.getSession().getAttribute("adminUser");
        if(adminUser==null){
            throw new NeedLoginException("请登录"+uri);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
