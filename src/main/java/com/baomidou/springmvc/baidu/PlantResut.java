package com.baomidou.springmvc.baidu;


/**
 * Created by Administrator on 2019/12/22.
 */
public class PlantResut {

    private String name;

    private Double score;

    private Baike baike_info;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Baike getBaike_info() {
        return baike_info;
    }

    public void setBaike_info(Baike baike_info) {
        this.baike_info = baike_info;
    }
}
