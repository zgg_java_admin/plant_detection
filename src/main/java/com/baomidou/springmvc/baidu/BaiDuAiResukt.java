package com.baomidou.springmvc.baidu;

import com.alibaba.fastjson.JSON;

import java.util.List;

/**
 * Created by Administrator on 2019/12/22.T
 */
public class BaiDuAiResukt {
    /**
     * 日志查询ID
     */
    private String log_id;

    private List result;

    public <T> T getOne(Class<T> clazz){
        if(result==null || result.isEmpty()){
            return null;
        }
       return JSON.parseObject(JSON.toJSONString(result.get(0)),clazz) ;
    }

    public <T> List<T> getList(Class<T> clazz){
        if(result==null || result.isEmpty()){
            return null;
        }
        return JSON.parseArray(JSON.toJSONString(result),clazz) ;
    }

    public String getLog_id() {
        return log_id;
    }

    public void setLog_id(String log_id) {
        this.log_id = log_id;
    }

    public List getResult() {
        return result;
    }

    public void setResult(List result) {
        this.result = result;
    }
}
