//package com.baomidou.springmvc.baidu;
//
//import org.json.JSONObject;
//
//public class SampleUtil {
//    //设置APPID/AK/SK
//    public static final String APP_ID = "你的 App ID";
//    public static final String API_KEY = "你的 Api Key";
//    public static final String SECRET_KEY = "你的 Secret Key";
//
//    public static void main(String[] args) {
//        // 初始化一个AipImageCensor
//        AipImageCensor client = new AipImageCensor(APP_ID, API_KEY, SECRET_KEY);
//
//        // 可选：设置log4j日志输出格式，若不设置，则使用默认配置
//        // 也可以直接通过jvm启动参数设置此环境变量
//        System.setProperty("aip.log4j.conf", "path/to/your/log4j.properties");
//
//        // 调用接口
//        String path = "test.jpg";
//        JSONObject res = client.antiPorn(path);
//        System.out.println(res.toString(2));
//
//    }
//}