package com.baomidou.springmvc.util;

import com.baidu.aip.contentcensor.AipContentCensor;
import com.baidu.aip.ocr.AipOcr;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;

public class AipImageCensorUtil {
    private static final String APP_ID = "18086755";

    private static final String API_KEY = "m0GtjiXzBtulnAWyPgip9vGM";

    private static final String SECRET_KEY = "s92Wc4ekgtjZMIF4XeRp565ZBoWklWqm";

    /**图片内容审核客户端*/
    private static AipContentCensor contentCensorClient;
    static {
        getClient();
    }

    /**
     * 初始化Client
     */
    private static void getClient() {

        //初始化图片审核客户端
        contentCensorClient = new AipContentCensor(APP_ID, API_KEY, SECRET_KEY);
        contentCensorClient.setConnectionTimeoutInMillis(2000);
        contentCensorClient.setSocketTimeoutInMillis(60000);

        // 初始化一个AipOcr
        AipOcr ocrClient = new AipOcr(APP_ID, API_KEY, SECRET_KEY);
        ocrClient.setConnectionTimeoutInMillis(2000);
        ocrClient.setSocketTimeoutInMillis(60000);
    }


    /**
     * 识别文本中的敏感词汇(需要对百度API增强)
     *
     * @param text
     */
    public static boolean discernSensitiveWords(String text, HashMap<String, String> options)   {
        String resp;
        try {
            //防止出现特殊符号，制造异常
            resp = contentCensorClient.antiSpam(URLDecoder.decode(text, "UTF-8"), options).toString();
        } catch (UnsupportedEncodingException e) {
            return false;
        }
        //标识审核是否通过的结果所在未知
        int len = resp.lastIndexOf("m\":") + 3;
        String spam = resp.substring(len,len + 1);
        //System.out.println(spam);
        if(spam.equals("0")) {
            return true;
        }else {
            return false;
        }
    }

    /**
     * 识别图中的敏感内容(需要对百度API增强)
     *
     * @param image
     */
    public static boolean discernSensitiveImage(byte[] image) throws IOException {

        // 参数为本地图片文件二进制数组
        String resp = contentCensorClient.imageCensorUserDefined(image, null).toString();
        //截取conclusionType的值
        String result = resp.substring(resp.length() - 2,resp.length()-1);
        if (result.equals("1")) {
            return true;
        }else  {
            return false;
        }

    }

    public static void main(String[] args) throws IOException {
        //文本内容审核
        boolean result1 = discernSensitiveWords("疆独", null);
        System.out.println("文本内容是否通过：" + result1);
//        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//        InputStream inputStream = new FileInputStream("F:\\img\\1.jpg");
//        byte[] b = new byte[inputStream.available()];
//        inputStream.read(b);
//        boolean result2 = discernSensitiveImage(b);
//        System.out.println("图片内容是否通过：" + result2);
    }



}
