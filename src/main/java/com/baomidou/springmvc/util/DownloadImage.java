package com.baomidou.springmvc.util;

import com.alibaba.fastjson.JSON;
import com.baomidou.springmvc.baidu.BaiDuAiResukt;
import com.baomidou.springmvc.controller.ImgController;
import com.baomidou.springmvc.util.plant.PlantUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.UUID;

public class DownloadImage {
    //文件临时目录 确保文件目录真实存在
    public static final String FILE_PATH_TEMP ="D:\\";

    public static void main(String[] args) throws Exception {
        download("https://dss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2883202075,4176349800&fm=26&gp=0.jpg");
    }

    /**
     * 下载图片并进行识别
     * @param urlString
     * @throws Exception
     */
    public static void download(String urlString) throws Exception {
        String filename = UUID.randomUUID().toString()+ urlString.substring(urlString.lastIndexOf("."));
        String savePath = FILE_PATH_TEMP+filename;
        // 构造URL
        URL url = new URL(urlString);
        // 打开连接
        URLConnection con = url.openConnection();
        //设置请求超时为5s
        con.setConnectTimeout(5*1000);
        // 输入流
        InputStream is = con.getInputStream();

        // 1K的数据缓冲
        byte[] bs = new byte[1024];
        // 读取到的数据长度
        int len;
        // 输出的文件流
        File sf=new File(savePath);
        sf.createNewFile();
        OutputStream os = new FileOutputStream(savePath);
        // 开始读取
        while ((len = is.read(bs)) != -1) {
            os.write(bs, 0, len);
        }
        // 完毕，关闭所有链接
        os.close();
        is.close();

        //开始ai识别
        BaiDuAiResukt baiDuAiResukt = PlantUtil.plant(sf.getPath());
        System.out.println("植株识别结果：" +JSON.toJSONString(baiDuAiResukt,true));
        baiDuAiResukt = AnimalUtil.animal(sf.getPath());
        System.out.println("害虫识别结果：" +JSON.toJSONString(baiDuAiResukt,true));
        baiDuAiResukt = IngredientUtil.ingredient(sf.getPath());
        System.out.println("果蔬识别结果：" +JSON.toJSONString(baiDuAiResukt,true));
        sf.delete();
    }
}

