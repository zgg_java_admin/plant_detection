package com.baomidou.springmvc.util.plant;

/**
 * Created by Administrator on 2019/12/22.
 */

import com.alibaba.fastjson.JSON;
import com.baomidou.springmvc.baidu.BaiDuAiResukt;
import com.baomidou.springmvc.baidu.PlantResut;
import com.baomidou.springmvc.util.*;

import java.net.URLEncoder;
import java.util.Map;

/**
 * 植物识别
 */
public class PlantUtil {

    /**
     * 重要提示代码中所需工具类
     * FileUtil,Base64Util,HttpUtil,GsonUtils请从
     * https://ai.baidu.com/file/658A35ABAB2D404FBF903F64D47C1F72
     * https://ai.baidu.com/file/C8D81F3301E24D2892968F09AE1AD6E2
     * https://ai.baidu.com/file/544D677F5D4E4F17B4122FBD60DB82B3
     * https://ai.baidu.com/file/470B3ACCA3FE43788B5A963BF0B625F3
     * 下载
     */
    public static BaiDuAiResukt plant(String filePath,String ... acToken) {
        if(acToken==null || acToken.length==0){
            acToken = new String[]{AccessTokenUtil.accessToken()};
        }
        // 请求url
        String url = "https://aip.baidubce.com/rest/2.0/image-classify/v1/plant";
        try {
            byte[] imgData = FileUtil.readFileByBytes(filePath);
            String imgStr = Base64Util.encode(imgData);
            String imgParam = URLEncoder.encode(imgStr, "UTF-8");
            String param = "image=" + imgParam+"&baike_num=3";
            // 注意这里仅为了简化编码每一次请求都去获取access_token，线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。
            String accessToken = acToken[0];
            String result = HttpUtil.post(url, accessToken, param);
            System.out.println(result);
            BaiDuAiResukt baiDuAiResukt = new BaiDuAiResukt();
            baiDuAiResukt = JSON.parseObject(result,baiDuAiResukt.getClass());
            System.out.println(baiDuAiResukt.getList(PlantResut.class));
            return baiDuAiResukt;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        BaiDuAiResukt plant =  PlantUtil.plant("F://1.png");
        System.out.println(plant);
    }
}