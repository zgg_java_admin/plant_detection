package com.baomidou.springmvc.model.plant;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.springmvc.table.ArticleCommentTable;


/**
 * 文章评论表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@TableName("article_comment")
public class ArticleCommentEntity extends BaseEntity{
  /**
   * 文章id
   */
  @TableField(ArticleCommentTable.ARTICLE_ID)
  private String articleId;
  /**
   * 
   */
  @TableField(ArticleCommentTable.USER_ID)
  private String userId;
  /**
   * 
   */
  @TableField(ArticleCommentTable.REPLY_ID)
  private String replyId;
  /**
   * 
   */
  @TableField(ArticleCommentTable.CONTENT)
  private String content;

  private String time;

  @TableField("r_content")
  private String rContent;

  @TableField(exist = false)
  private String userName;

  public String getArticleId() {
    return articleId;
  }

  public void setArticleId(String articleId) {
    this.articleId = articleId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getReplyId() {
    return replyId;
  }

  public void setReplyId(String replyId) {
    this.replyId = replyId;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public String getrContent() {
    return rContent;
  }

  public void setrContent(String rContent) {
    this.rContent = rContent;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }
}
