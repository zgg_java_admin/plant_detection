package com.baomidou.springmvc.model.plant;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.springmvc.table.UserTable;


/**
 * 系统用户表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@TableName("user")
public class UserEntity extends BaseEntity{
  /**
   * 用户名
   */
  @TableField(UserTable.NAME)
  private String name;
  /**
   * 0 未审核  1 已审核  2 已禁用  
   */
  @TableField(UserTable.TYPE)
  private Integer type;
  /**
   * 用户邮箱登录的时候使用
   */
  @TableField(UserTable.EAMIL)
  private String eamil;
  /**
   * 
   */
  @TableField(UserTable.SALT)
  private String salt;
  /**
   * 用户密码
   */
  @TableField(UserTable.PWD)
  private String pwd;
  /**
   * 最后登录的时间
   */
  @TableField(UserTable.LAST_LOGIN_TIME)
  private String lastLoginTime;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public String getEamil() {
    return eamil;
  }

  public void setEamil(String eamil) {
    this.eamil = eamil;
  }

  public String getSalt() {
    return salt;
  }

  public void setSalt(String salt) {
    this.salt = salt;
  }

  public String getPwd() {
    return pwd;
  }

  public void setPwd(String pwd) {
    this.pwd = pwd;
  }

  public String getLastLoginTime() {
    return lastLoginTime;
  }

  public void setLastLoginTime(String lastLoginTime) {
    this.lastLoginTime = lastLoginTime;
  }
}
