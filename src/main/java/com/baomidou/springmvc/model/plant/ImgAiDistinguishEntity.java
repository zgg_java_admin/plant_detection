package com.baomidou.springmvc.model.plant;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.springmvc.table.ImgAiDistinguishTable;


/**
 * 百度AI识别结果管理
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@TableName("img_ai_distinguish")
public class ImgAiDistinguishEntity extends BaseEntity{
  /**
   * 图片识别地址
   */
  @TableField(ImgAiDistinguishTable.IMG_URL)
  private String imgUrl;
  /**
   * 
   */
  @TableField(ImgAiDistinguishTable.USER_ID)
  private String userId;
  /**
   * 识别类型
   */
  @TableField(ImgAiDistinguishTable.TYPE)
  private Integer type;
  /**
   * 识别结果
   */
  @TableField(ImgAiDistinguishTable.RESULT)
  private String result;
  /**
   * 识别时间
   */
  @TableField("time")
  private String time;

  @TableField(exist = false)
  private String userName;

  public String getImgUrl() {
    return imgUrl;
  }

  public void setImgUrl(String imgUrl) {
    this.imgUrl = imgUrl;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public String getResult() {
    return result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }
}
