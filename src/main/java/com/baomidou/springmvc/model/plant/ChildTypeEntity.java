package com.baomidou.springmvc.model.plant;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.springmvc.table.ChildTypeTable;


/**
 * 二级分类表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@TableName("child_type")
public class ChildTypeEntity extends BaseEntity{
  /**
   * 名称
   */
  @TableField(ChildTypeTable.NAME)
  private String name;
  /**
   * 所属的一级分类  
   */
  @TableField(ChildTypeTable.TYPE)
  private Integer type;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }
}
