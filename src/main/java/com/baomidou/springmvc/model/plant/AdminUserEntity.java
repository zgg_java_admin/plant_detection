package com.baomidou.springmvc.model.plant;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.springmvc.table.AdminUserTable;


/**
 * 
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@TableName("admin_user")
public class AdminUserEntity extends BaseEntity{
  /**
   * 登录名
   */
  @TableField(AdminUserTable.LOGIN_NAME)
  private String loginName;
  /**
   * 密码
   */
  @TableField(AdminUserTable.PWD)
  private String pwd;
  /**
   * 名称
   */
  @TableField(AdminUserTable.NAME)
  private String name;

  /**
   * 创建时间
   */
  @TableField("create_time")
  private String createTime;

  public String getLoginName() {
    return loginName;
  }

  public void setLoginName(String loginName) {
    this.loginName = loginName;
  }

  public String getPwd() {
    return pwd;
  }

  public void setPwd(String pwd) {
    this.pwd = pwd;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCreateTime() {
    return createTime;
  }

  public void setCreateTime(String createTime) {
    this.createTime = createTime;
  }
}
