package com.baomidou.springmvc.model.plant;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.springmvc.table.UserReadArticleTable;
import java.util.Date;


/**
 * 用户浏览文章表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@TableName("user_read_article")
public class UserReadArticleEntity extends BaseEntity{
  /**
   * 用户ID
   */
  @TableField(UserReadArticleTable.USER_ID)
  private String userId;
  /**
   * 
   */
  @TableField(UserReadArticleTable.ARTICLE_ID)
  private String articleId;
  /**
   * 浏览时间
   */
  @TableField(UserReadArticleTable.TIME)
  private Date time;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getArticleId() {
    return articleId;
  }

  public void setArticleId(String articleId) {
    this.articleId = articleId;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }
}
