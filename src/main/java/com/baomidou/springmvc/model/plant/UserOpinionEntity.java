package com.baomidou.springmvc.model.plant;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.springmvc.table.UserOpinionTable;


/**
 * 用户意见表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@TableName("user_opinion")
public class UserOpinionEntity extends BaseEntity{
  /**
   * 
   */
  @TableField(UserOpinionTable.USER_ID)
  private String userId;
  /**
   * 内容 
   */
  @TableField(UserOpinionTable.CONTENT)
  private String content;
  /**
   * 回复
   */
  @TableField(UserOpinionTable.REPALY)
  private String repaly;
  /**
   * 提交时间
   */
  @TableField(UserOpinionTable.TIME)
  private String time;

  @TableField("repaly_flag")
  private String repalyFlag;

  @TableField(exist = false)
  private String userName;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getRepaly() {
    return repaly;
  }

  public void setRepaly(String repaly) {
    this.repaly = repaly;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public String getRepalyFlag() {
    return repalyFlag;
  }

  public void setRepalyFlag(String repalyFlag) {
    this.repalyFlag = repalyFlag;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }
}
