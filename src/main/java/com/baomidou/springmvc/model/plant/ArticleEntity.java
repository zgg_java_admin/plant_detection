package com.baomidou.springmvc.model.plant;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.springmvc.table.ArticleTable;


/**
 * 文章表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@TableName("article")
public class ArticleEntity extends BaseEntity{
  /**
   * 标题
   */
  @TableField(ArticleTable.TITLE)
  private String title;
  /**
   * 摘要
   */
  @TableField(ArticleTable.SUMMARY)
  private String summary;
  /**
   * 内容
   */
  @TableField(ArticleTable.CONTENT)
  private String content;
  /**
   * 1 : 植被 2害虫 3果蔬
   */
  @TableField(ArticleTable.TYPE)
  private Integer type;
  /**
   * 二级分类ID
   */
  @TableField(ArticleTable.CHILD_TYPE)
  private String childType;

  @TableField(exist = false)
  private String childName;

  /**
   * 标签
   */
  @TableField(ArticleTable.LABEL)
  private String label;
  /**
   * 用户ID
   */
  @TableField(ArticleTable.USER_ID)
  private String userId;

  @TableField(exist = false)
  private String userName;


  /**
   * 发表时间
   */
  @TableField(ArticleTable.TIME)
  private String time;
  /**
   * 1 可用 2不可用
   */
  @TableField(ArticleTable.STATUS)
  private Integer status;
  /**
   * 检查结果
   */
  @TableField(ArticleTable.CHECK_RESULT)
  private String checkResult;

  /**
   * 是否置顶
   */
  private Boolean top;

  /**
   * 封面地址
   */
  private String cover;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public String getChildType() {
    return childType;
  }

  public void setChildType(String childType) {
    this.childType = childType;
  }

  public String getChildName() {
    return childName;
  }

  public void setChildName(String childName) {
    this.childName = childName;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getCheckResult() {
    return checkResult;
  }

  public void setCheckResult(String checkResult) {
    this.checkResult = checkResult;
  }

  public Boolean getTop() {
    return top;
  }

  public void setTop(Boolean top) {
    this.top = top;
  }

  public String getCover() {
    return cover;
  }

  public void setCover(String cover) {
    this.cover = cover;
  }
}
