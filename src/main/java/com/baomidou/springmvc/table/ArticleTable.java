package com.baomidou.springmvc.table;

/**
 * 文章表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
public class ArticleTable {
  /**
   * 标题
   */
  public static final String TITLE = "title";
  /**
   * 摘要
   */
  public static final String SUMMARY = "summary";
  /**
   * 内容
   */
  public static final String CONTENT = "content";
  /**
   * 1 : 植被 2害虫 3果蔬
   */
  public static final String TYPE = "type";
  /**
   * 二级分类ID
   */
  public static final String CHILD_TYPE = "child_type";
  /**
   * 标签
   */
  public static final String LABEL = "label";
  /**
   * 多发地
   */
  public static final String FO_AREA = "fo_area";
  /**
   * 致死植被
   */
  public static final String LETHAL_VEGETATION = "lethal_vegetation";
  /**
   * 解决办法
   */
  public static final String TERMS_OF_SETTLEMENT = "terms_of_settlement";
  /**
   * 
   */
  public static final String USER_ID = "user_id";
  /**
   * 
   */
  public static final String TIME = "time";
  /**
   * 1 可用 2不可用
   */
  public static final String STATUS = "status";
  /**
   * 检查结果
   */
  public static final String CHECK_RESULT = "check_result";
}
