package com.baomidou.springmvc.table;

/**
 * 
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
public class AdminUserTable {
  /**
   * 登录名
   */
  public static final String LOGIN_NAME = "login_name";
  /**
   * 密码
   */
  public static final String PWD = "pwd";
  /**
   * 名称
   */
  public static final String NAME = "name";
}
