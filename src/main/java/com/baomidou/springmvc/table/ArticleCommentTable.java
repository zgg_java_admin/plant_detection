package com.baomidou.springmvc.table;

/**
 * 文章评论表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
public class ArticleCommentTable {
  /**
   * 文章id
   */
  public static final String ARTICLE_ID = "article_id";
  /**
   * 
   */
  public static final String USER_ID = "user_id";
  /**
   * 
   */
  public static final String REPLY_ID = "reply_id";
  /**
   * 
   */
  public static final String CONTENT = "content";
  /**
   * 1 评论  2回复
   */
  public static final String TYPE = "type";
  /**
   * 评论所有的上级ID集合
   */
  public static final String P_IDS = "p_ids";
}
