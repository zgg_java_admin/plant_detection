package com.baomidou.springmvc.table;

/**
 * 用户意见表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
public class UserOpinionTable {
  /**
   * 
   */
  public static final String USER_ID = "user_id";
  /**
   * 内容 
   */
  public static final String CONTENT = "content";
  /**
   * 回复
   */
  public static final String REPALY = "repaly";
  /**
   * 提交时间
   */
  public static final String TIME = "time";
}
