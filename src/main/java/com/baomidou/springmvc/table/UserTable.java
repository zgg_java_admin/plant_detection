package com.baomidou.springmvc.table;

/**
 * 系统用户表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
public class UserTable {
  /**
   * 用户名
   */
  public static final String NAME = "name";
  /**
   * 0 未审核  1 已审核  2 已禁用  
   */
  public static final String TYPE = "type";
  /**
   * 用户邮箱登录的时候使用
   */
  public static final String EAMIL = "eamil";
  /**
   * 
   */
  public static final String SALT = "salt";
  /**
   * 用户密码
   */
  public static final String PWD = "pwd";
  /**
   * 最后登录的时间
   */
  public static final String LAST_LOGIN_TIME = "last_login_time";
}
