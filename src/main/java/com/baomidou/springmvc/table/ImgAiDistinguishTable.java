package com.baomidou.springmvc.table;

/**
 * 百度AI识别结果管理
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
public class ImgAiDistinguishTable {
  /**
   * 图片识别地址
   */
  public static final String IMG_URL = "img_url";
  /**
   * 
   */
  public static final String USER_ID = "user_id";
  /**
   * 识别类型
   */
  public static final String TYPE = "type";
  /**
   * 识别结果
   */
  public static final String RESULT = "result";
}
