package com.baomidou.springmvc.table;

/**
 * 二级分类表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
public class ChildTypeTable {
  /**
   * 名称
   */
  public static final String NAME = "name";
  /**
   * 所属的一级分类  
   */
  public static final String TYPE = "type";
}
