package com.baomidou.springmvc.table;

/**
 * 用户浏览文章表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
public class UserReadArticleTable {
  /**
   * 用户ID
   */
  public static final String USER_ID = "user_id";
  /**
   * 
   */
  public static final String ARTICLE_ID = "article_id";
  /**
   * 浏览时间
   */
  public static final String TIME = "time";
}
