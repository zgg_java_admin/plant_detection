package com.baomidou.springmvc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springmvc.model.plant.AdminUserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@Mapper
public interface AdminUserDao extends BaseMapper<AdminUserEntity> {
}