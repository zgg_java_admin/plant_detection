package com.baomidou.springmvc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springmvc.model.plant.ChildTypeEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 二级分类表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@Mapper
public interface ChildTypeDao extends BaseMapper<ChildTypeEntity> {
}