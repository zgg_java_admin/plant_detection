package com.baomidou.springmvc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springmvc.model.plant.ArticleEntity;
import com.baomidou.springmvc.model.plant.UserReadArticleEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 用户浏览文章表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@Mapper
public interface UserReadArticleDao extends BaseMapper<UserReadArticleEntity> {


    @Select("SELECT * FROM article WHERE type =1 and id in(SELECT article_id from user_read_article WHERE user_id = #{userId})")
    List<ArticleEntity> selectByUserId(String userId);

}