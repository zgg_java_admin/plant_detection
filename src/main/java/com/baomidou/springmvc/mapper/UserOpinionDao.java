package com.baomidou.springmvc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springmvc.model.plant.UserOpinionEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户意见表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@Mapper
public interface UserOpinionDao extends BaseMapper<UserOpinionEntity> {
}