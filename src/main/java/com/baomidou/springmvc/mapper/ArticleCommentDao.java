package com.baomidou.springmvc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springmvc.model.plant.ArticleCommentEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文章评论表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@Mapper
public interface ArticleCommentDao extends BaseMapper<ArticleCommentEntity> {
}