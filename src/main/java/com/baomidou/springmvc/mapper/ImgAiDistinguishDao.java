package com.baomidou.springmvc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springmvc.model.plant.ImgAiDistinguishEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 百度AI识别结果管理
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@Mapper
public interface ImgAiDistinguishDao extends BaseMapper<ImgAiDistinguishEntity> {
}