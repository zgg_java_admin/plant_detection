package com.baomidou.springmvc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springmvc.model.plant.UserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统用户表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {
}