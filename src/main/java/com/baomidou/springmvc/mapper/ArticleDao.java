package com.baomidou.springmvc.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springmvc.model.plant.ArticleEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 文章表
 * @author zlm
 * @date 2019-12-23 11:24:31
 */
@Mapper
public interface ArticleDao extends BaseMapper<ArticleEntity> {

    @Select("select * from article WHERE `status` = 1  AND user_id = '-1'")
    List<ArticleEntity> listll();

    @Select("select * from article WHERE `status` = 1  AND user_id = '-1' and child_type ='${childType}' ")
    List<ArticleEntity> listll(@Param("childType") String childType );
 }